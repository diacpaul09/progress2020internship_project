 
/*------------------------------------------------------------------------
   File        : ViewCustomers
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Fri Nov 13 23:33:12 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewOrdersOfCustomers.ViewOrders FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewCustomers.ViewCustomers INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    {D:\Progress2020Internship_Project\src\Shared\dsOrder.i}
    DEFINE PRIVATE VARIABLE bsCustomers                       AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnClose AS System.Windows.Forms.Button NO-UNDO.
    DEFINE PRIVATE VARIABLE components                        AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE custNumDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                     AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE userNameDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE nameDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE daCustomers                       AS daCustomers                                    NO-UNDO.
    DEFINE PRIVATE VARIABLE viewOrders                        AS ViewOrders                                     NO-UNDO.

    CONSTRUCTOR PUBLIC ViewCustomers (  ):
        
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        daCustomers = NEW daCustomers().
        daCustomers:FetchData(OUTPUT dataset dsCustomer BY-REFERENCE).
        bsCustomers:handle = DATASET dsCustomer:HANDLE.
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

	/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
	@VisualDesigner.
	METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
		DELETE OBJECT THIS-OBJECT.

	END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_MouseClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.MouseEventArgs ):
        viewOrders = NEW ViewOrders(ttCustomer.CustNum, ttCustomer.Name).
        viewOrders:Show().

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc1 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc1 = NEW Progress.Data.TableDesc("ttCustomer").
        THIS-OBJECT:bsCustomers = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:custNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:nameDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:userNameDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        CAST(THIS-OBJECT:bsCustomers, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* bsCustomers */
        /*  */
        THIS-OBJECT:bsCustomers:MaxDataGuess = 0.
        THIS-OBJECT:bsCustomers:NoLOBs = FALSE.
        THIS-OBJECT:bsCustomers:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar0 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc1:ChildTables = arrayvar0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS Progress.Data.ColumnPropDesc EXTENT 3 NO-UNDO.
        arrayvar1[1] = NEW Progress.Data.ColumnPropDesc("CustNum", "Cust Num", Progress.Data.DataType:INTEGER).
        arrayvar1[2] = NEW Progress.Data.ColumnPropDesc("Name", "Name", Progress.Data.DataType:CHARACTER).
        arrayvar1[3] = NEW Progress.Data.ColumnPropDesc("User_Name", "UserName", Progress.Data.DataType:CHARACTER).
        tableDesc1:Columns = arrayvar1.
        THIS-OBJECT:bsCustomers:TableSchema = tableDesc1.
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS System.Windows.Forms.DataGridViewColumn EXTENT 3 NO-UNDO.
        arrayvar2[1] = THIS-OBJECT:custNumDataGridViewTextBoxColumn.
        arrayvar2[2] = THIS-OBJECT:nameDataGridViewTextBoxColumn.
        arrayvar2[3] = THIS-OBJECT:userNameDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar2).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bsCustomers.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(12, 12).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(360, 469).
        THIS-OBJECT:dataGridView1:TabIndex = 0.
        THIS-OBJECT:dataGridView1:MouseClick:Subscribe(THIS-OBJECT:dataGridView1_MouseClick).
        /*  */
        /* custNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:DataPropertyName = "CustNum".
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:HeaderText = "Cust Num".
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:Name = "custNumDataGridViewTextBoxColumn".
        /*  */
        /* nameDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:nameDataGridViewTextBoxColumn:DataPropertyName = "Name".
        THIS-OBJECT:nameDataGridViewTextBoxColumn:HeaderText = "Name".
        THIS-OBJECT:nameDataGridViewTextBoxColumn:Name = "nameDataGridViewTextBoxColumn".
        /*  */
        /* userNameDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:userNameDataGridViewTextBoxColumn:DataPropertyName = "User_Name".
        THIS-OBJECT:userNameDataGridViewTextBoxColumn:HeaderText = "UserName".
        THIS-OBJECT:userNameDataGridViewTextBoxColumn:Name = "userNameDataGridViewTextBoxColumn".
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(39, 506).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(307, 73).
        THIS-OBJECT:btnClose:TabIndex = 1.
        THIS-OBJECT:btnClose:Text = "Close".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* ViewCustomers */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(384, 591).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "ViewCustomers".
        THIS-OBJECT:Text = "ViewCustomers".
        CAST(THIS-OBJECT:bsCustomers, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC ViewCustomers ( ):

    END DESTRUCTOR.

END CLASS.