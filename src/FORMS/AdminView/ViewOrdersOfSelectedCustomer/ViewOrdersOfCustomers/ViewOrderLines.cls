 
/*------------------------------------------------------------------------
   File        : ViewOrderLines
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Tue Nov 17 18:09:43 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.BusinessEntity.beOrderLine FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewOrdersOfCustomers.ViewOrderLines INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsOrderline.i}
    DEFINE PRIVATE VARIABLE bsOrderlines                             AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnClose                                 AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE components                               AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE qtyDataGridViewTextBoxColumn             AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE priceDataGridViewTextBoxColumn           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE orderNumDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE orderLineStatusDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE lineNumDataGridViewTextBoxColumn         AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE itemNumDataGridViewTextBoxColumn         AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE extendedPriceDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE discountDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                            AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE beorderLines                             AS beOrderLine                                    NO-UNDO.

    CONSTRUCTOR PUBLIC ViewOrderLines ( INPUT ordernumber AS INTEGER ):
        
        InitializeComponent().
        beorderLines = NEW beOrderLine().
        beorderLines:getOrderLines(ordernumber, INPUT-OUTPUT DATASET dsORderline BY-REFERENCE).
        
        bsOrderlines:handle = DATASET dsORderline:HANDLE.
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc1 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc1 = NEW Progress.Data.TableDesc("ttOrderLine").
        THIS-OBJECT:bsOrderlines = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:discountDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:extendedPriceDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:lineNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:orderLineStatusDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:priceDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:qtyDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        CAST(THIS-OBJECT:bsOrderlines, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* bsOrderlines */
        /*  */
        THIS-OBJECT:bsOrderlines:MaxDataGuess = 0.
        THIS-OBJECT:bsOrderlines:NoLOBs = FALSE.
        THIS-OBJECT:bsOrderlines:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar0 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc1:ChildTables = arrayvar0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS Progress.Data.ColumnPropDesc EXTENT 8 NO-UNDO.
        arrayvar1[1] = NEW Progress.Data.ColumnPropDesc("Discount", "Discount", Progress.Data.DataType:INTEGER).
        arrayvar1[2] = NEW Progress.Data.ColumnPropDesc("ExtendedPrice", "Extended Price", Progress.Data.DataType:DECIMAL).
        arrayvar1[3] = NEW Progress.Data.ColumnPropDesc("ItemNum", "Item Num", Progress.Data.DataType:INTEGER).
        arrayvar1[4] = NEW Progress.Data.ColumnPropDesc("LineNum", "Line Num", Progress.Data.DataType:INTEGER).
        arrayvar1[5] = NEW Progress.Data.ColumnPropDesc("OrderLineStatus", "Order Line Status", Progress.Data.DataType:CHARACTER).
        arrayvar1[6] = NEW Progress.Data.ColumnPropDesc("OrderNum", "Order Num", Progress.Data.DataType:INTEGER).
        arrayvar1[7] = NEW Progress.Data.ColumnPropDesc("Price", "Price", Progress.Data.DataType:DECIMAL).
        arrayvar1[8] = NEW Progress.Data.ColumnPropDesc("Qty", "Qty", Progress.Data.DataType:INTEGER).
        tableDesc1:Columns = arrayvar1.
        THIS-OBJECT:bsOrderlines:TableSchema = tableDesc1.
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS System.Windows.Forms.DataGridViewColumn EXTENT 8 NO-UNDO.
        arrayvar2[1] = THIS-OBJECT:discountDataGridViewTextBoxColumn.
        arrayvar2[2] = THIS-OBJECT:extendedPriceDataGridViewTextBoxColumn.
        arrayvar2[3] = THIS-OBJECT:itemNumDataGridViewTextBoxColumn.
        arrayvar2[4] = THIS-OBJECT:lineNumDataGridViewTextBoxColumn.
        arrayvar2[5] = THIS-OBJECT:orderLineStatusDataGridViewTextBoxColumn.
        arrayvar2[6] = THIS-OBJECT:orderNumDataGridViewTextBoxColumn.
        arrayvar2[7] = THIS-OBJECT:priceDataGridViewTextBoxColumn.
        arrayvar2[8] = THIS-OBJECT:qtyDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar2).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bsOrderlines.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(12, 12).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(845, 261).
        THIS-OBJECT:dataGridView1:TabIndex = 0.
        /*  */
        /* discountDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:discountDataGridViewTextBoxColumn:DataPropertyName = "Discount".
        THIS-OBJECT:discountDataGridViewTextBoxColumn:HeaderText = "Discount".
        THIS-OBJECT:discountDataGridViewTextBoxColumn:Name = "discountDataGridViewTextBoxColumn".
        /*  */
        /* extendedPriceDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:extendedPriceDataGridViewTextBoxColumn:DataPropertyName = "ExtendedPrice".
        THIS-OBJECT:extendedPriceDataGridViewTextBoxColumn:HeaderText = "Extended Price".
        THIS-OBJECT:extendedPriceDataGridViewTextBoxColumn:Name = "extendedPriceDataGridViewTextBoxColumn".
        /*  */
        /* itemNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn:DataPropertyName = "ItemNum".
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn:HeaderText = "Item Num".
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn:Name = "itemNumDataGridViewTextBoxColumn".
        /*  */
        /* lineNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:lineNumDataGridViewTextBoxColumn:DataPropertyName = "LineNum".
        THIS-OBJECT:lineNumDataGridViewTextBoxColumn:HeaderText = "Line Num".
        THIS-OBJECT:lineNumDataGridViewTextBoxColumn:Name = "lineNumDataGridViewTextBoxColumn".
        /*  */
        /* orderLineStatusDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:orderLineStatusDataGridViewTextBoxColumn:DataPropertyName = "OrderLineStatus".
        THIS-OBJECT:orderLineStatusDataGridViewTextBoxColumn:HeaderText = "Order Line Status".
        THIS-OBJECT:orderLineStatusDataGridViewTextBoxColumn:Name = "orderLineStatusDataGridViewTextBoxColumn".
        /*  */
        /* orderNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn:DataPropertyName = "OrderNum".
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn:HeaderText = "Order Num".
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn:Name = "orderNumDataGridViewTextBoxColumn".
        /*  */
        /* priceDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:priceDataGridViewTextBoxColumn:DataPropertyName = "Price".
        THIS-OBJECT:priceDataGridViewTextBoxColumn:HeaderText = "Price".
        THIS-OBJECT:priceDataGridViewTextBoxColumn:Name = "priceDataGridViewTextBoxColumn".
        /*  */
        /* qtyDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:qtyDataGridViewTextBoxColumn:DataPropertyName = "Qty".
        THIS-OBJECT:qtyDataGridViewTextBoxColumn:HeaderText = "Qty".
        THIS-OBJECT:qtyDataGridViewTextBoxColumn:Name = "qtyDataGridViewTextBoxColumn".
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(292, 351).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(288, 66).
        THIS-OBJECT:btnClose:TabIndex = 1.
        THIS-OBJECT:btnClose:Text = "Close".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* ViewOrderLines */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(872, 446).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "ViewOrderLines".
        THIS-OBJECT:Text = "ViewOrderLines".
        CAST(THIS-OBJECT:bsOrderlines, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC ViewOrderLines ( ):

    END DESTRUCTOR.

END CLASS.