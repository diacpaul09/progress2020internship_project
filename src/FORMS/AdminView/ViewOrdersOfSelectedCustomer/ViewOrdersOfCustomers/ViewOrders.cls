 
/*------------------------------------------------------------------------
   File        : ViewOrders
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Fri Nov 13 23:44:52 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.DataAcces.Order.daOrder FROM PROPATH.
USING src.Classes.BusinessEntity.beOrders FROM PROPATH.
USING src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewOrdersOfCustomers.ViewOrderLines FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewOrdersOfCustomers.ViewOrders INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    {D:\Progress2020Internship_Project\src\Shared\dsOrder.i}
    DEFINE PRIVATE VARIABLE bsOrders                             AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnClose                             AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE carrierDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE components                           AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE label2                               AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE label1                               AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE shipDateDataGridViewTextBoxColumn    AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE promiseDateDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE orderStatusDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE orderNumDataGridViewTextBoxColumn    AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE orderDateDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                        AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE custNumDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE creditCardDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE daOrders                             AS daOrder                                        NO-UNDO.
    DEFINE PRIVATE VARIABLE beOrders                             AS beOrders                                       NO-UNDO.
    DEFINE PRIVATE VARIABLE viewOrderlines                       AS ViewOrderLines                                 NO-UNDO.

    CONSTRUCTOR PUBLIC ViewOrders ( INPUT custonumber AS INTEGER ,INPUT customername AS CHARACTER):
        
        InitializeComponent().
        
        label2:text = customername + ":".
        
        DATASET dsOrder:EMPTY-DATASET ().
        beOrders = NEW beOrders().
        beOrders:getOrders(OUTPUT dataset dsOrder BY-REFERENCE, custonumber).
        
        bsOrders:Handle = DATASET dsOrder:HANDLE.
        
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_MouseClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.MouseEventArgs ):
        viewOrderlines = NEW ViewOrderLines(ttOrder.OrderNum).
        viewOrderlines:Show().

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc2 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc2 = NEW Progress.Data.TableDesc("ttOrder").
        THIS-OBJECT:bsOrders = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:custNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:orderStatusDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:creditCardDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:carrierDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:orderDateDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:promiseDateDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:shipDateDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        THIS-OBJECT:label1 = NEW System.Windows.Forms.Label().
        THIS-OBJECT:label2 = NEW System.Windows.Forms.Label().
        CAST(THIS-OBJECT:bsOrders, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* bsOrders */
        /*  */
        THIS-OBJECT:bsOrders:MaxDataGuess = 0.
        THIS-OBJECT:bsOrders:NoLOBs = FALSE.
        THIS-OBJECT:bsOrders:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar0 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc2:ChildTables = arrayvar0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS Progress.Data.ColumnPropDesc EXTENT 15 NO-UNDO.
        arrayvar1[1] = NEW Progress.Data.ColumnPropDesc("BillToID", "Bill To ID", Progress.Data.DataType:INTEGER).
        arrayvar1[2] = NEW Progress.Data.ColumnPropDesc("Carrier", "Carrier", Progress.Data.DataType:CHARACTER).
        arrayvar1[3] = NEW Progress.Data.ColumnPropDesc("CreditCard", "Credit Card", Progress.Data.DataType:CHARACTER).
        arrayvar1[4] = NEW Progress.Data.ColumnPropDesc("CustNum", "Cust Num", Progress.Data.DataType:INTEGER).
        arrayvar1[5] = NEW Progress.Data.ColumnPropDesc("Instructions", "Instructions", Progress.Data.DataType:CHARACTER).
        arrayvar1[6] = NEW Progress.Data.ColumnPropDesc("OrderDate", "Ordered", Progress.Data.DataType:DATE).
        arrayvar1[7] = NEW Progress.Data.ColumnPropDesc("OrderNum", "Order Num", Progress.Data.DataType:INTEGER).
        arrayvar1[8] = NEW Progress.Data.ColumnPropDesc("OrderStatus", "Order Status", Progress.Data.DataType:CHARACTER).
        arrayvar1[9] = NEW Progress.Data.ColumnPropDesc("PO", "PO", Progress.Data.DataType:CHARACTER).
        arrayvar1[10] = NEW Progress.Data.ColumnPropDesc("PromiseDate", "Promised", Progress.Data.DataType:DATE).
        arrayvar1[11] = NEW Progress.Data.ColumnPropDesc("SalesRep", "Sales Rep", Progress.Data.DataType:CHARACTER).
        arrayvar1[12] = NEW Progress.Data.ColumnPropDesc("ShipDate", "Shipped", Progress.Data.DataType:DATE).
        arrayvar1[13] = NEW Progress.Data.ColumnPropDesc("ShipToID", "Ship To ID", Progress.Data.DataType:INTEGER).
        arrayvar1[14] = NEW Progress.Data.ColumnPropDesc("Terms", "Terms", Progress.Data.DataType:CHARACTER).
        arrayvar1[15] = NEW Progress.Data.ColumnPropDesc("WarehouseNum", "Warehouse Num", Progress.Data.DataType:INTEGER).
        tableDesc2:Columns = arrayvar1.
        THIS-OBJECT:bsOrders:TableSchema = tableDesc2.
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS System.Windows.Forms.DataGridViewColumn EXTENT 8 NO-UNDO.
        arrayvar2[1] = THIS-OBJECT:custNumDataGridViewTextBoxColumn.
        arrayvar2[2] = THIS-OBJECT:orderNumDataGridViewTextBoxColumn.
        arrayvar2[3] = THIS-OBJECT:orderStatusDataGridViewTextBoxColumn.
        arrayvar2[4] = THIS-OBJECT:creditCardDataGridViewTextBoxColumn.
        arrayvar2[5] = THIS-OBJECT:carrierDataGridViewTextBoxColumn.
        arrayvar2[6] = THIS-OBJECT:orderDateDataGridViewTextBoxColumn.
        arrayvar2[7] = THIS-OBJECT:promiseDateDataGridViewTextBoxColumn.
        arrayvar2[8] = THIS-OBJECT:shipDateDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar2).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bsOrders.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(33, 78).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(846, 308).
        THIS-OBJECT:dataGridView1:TabIndex = 0.
        THIS-OBJECT:dataGridView1:MouseClick:Subscribe(THIS-OBJECT:dataGridView1_MouseClick).
        /*  */
        /* custNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:DataPropertyName = "CustNum".
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:HeaderText = "Cust Num".
        THIS-OBJECT:custNumDataGridViewTextBoxColumn:Name = "custNumDataGridViewTextBoxColumn".
        /*  */
        /* orderNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn:DataPropertyName = "OrderNum".
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn:HeaderText = "Order Num".
        THIS-OBJECT:orderNumDataGridViewTextBoxColumn:Name = "orderNumDataGridViewTextBoxColumn".
        /*  */
        /* orderStatusDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:orderStatusDataGridViewTextBoxColumn:DataPropertyName = "OrderStatus".
        THIS-OBJECT:orderStatusDataGridViewTextBoxColumn:HeaderText = "Order Status".
        THIS-OBJECT:orderStatusDataGridViewTextBoxColumn:Name = "orderStatusDataGridViewTextBoxColumn".
        /*  */
        /* creditCardDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:creditCardDataGridViewTextBoxColumn:DataPropertyName = "CreditCard".
        THIS-OBJECT:creditCardDataGridViewTextBoxColumn:HeaderText = "Credit Card".
        THIS-OBJECT:creditCardDataGridViewTextBoxColumn:Name = "creditCardDataGridViewTextBoxColumn".
        /*  */
        /* carrierDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:carrierDataGridViewTextBoxColumn:DataPropertyName = "Carrier".
        THIS-OBJECT:carrierDataGridViewTextBoxColumn:HeaderText = "Carrier".
        THIS-OBJECT:carrierDataGridViewTextBoxColumn:Name = "carrierDataGridViewTextBoxColumn".
        /*  */
        /* orderDateDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:orderDateDataGridViewTextBoxColumn:DataPropertyName = "OrderDate".
        THIS-OBJECT:orderDateDataGridViewTextBoxColumn:HeaderText = "Ordered".
        THIS-OBJECT:orderDateDataGridViewTextBoxColumn:Name = "orderDateDataGridViewTextBoxColumn".
        /*  */
        /* promiseDateDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:promiseDateDataGridViewTextBoxColumn:DataPropertyName = "PromiseDate".
        THIS-OBJECT:promiseDateDataGridViewTextBoxColumn:HeaderText = "Promised".
        THIS-OBJECT:promiseDateDataGridViewTextBoxColumn:Name = "promiseDateDataGridViewTextBoxColumn".
        /*  */
        /* shipDateDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:shipDateDataGridViewTextBoxColumn:DataPropertyName = "ShipDate".
        THIS-OBJECT:shipDateDataGridViewTextBoxColumn:HeaderText = "Shipped".
        THIS-OBJECT:shipDateDataGridViewTextBoxColumn:Name = "shipDateDataGridViewTextBoxColumn".
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(327, 418).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(260, 51).
        THIS-OBJECT:btnClose:TabIndex = 1.
        THIS-OBJECT:btnClose:Text = "Close".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* label1 */
        /*  */
        THIS-OBJECT:label1:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(11.25), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:label1:Location = NEW System.Drawing.Point(33, 27).
        THIS-OBJECT:label1:Name = "label1".
        THIS-OBJECT:label1:Size = NEW System.Drawing.Size(302, 23).
        THIS-OBJECT:label1:TabIndex = 2.
        THIS-OBJECT:label1:Text = "Here you have listed all the orders made by ".
        THIS-OBJECT:label1:UseCompatibleTextRendering = TRUE.
        /*  */
        /* label2 */
        /*  */
        THIS-OBJECT:label2:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(11.25), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:label2:Location = NEW System.Drawing.Point(321, 27).
        THIS-OBJECT:label2:Name = "label2".
        THIS-OBJECT:label2:Size = NEW System.Drawing.Size(361, 23).
        THIS-OBJECT:label2:TabIndex = 3.
        THIS-OBJECT:label2:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:label2:Click:Subscribe(THIS-OBJECT:label2_Click).
        /*  */
        /* ViewOrders */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(917, 481).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:label2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:label1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "ViewOrders".
        THIS-OBJECT:Text = "ViewOrders".
        CAST(THIS-OBJECT:bsOrders, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID label2_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC ViewOrders ( ):

    END DESTRUCTOR.

END CLASS.