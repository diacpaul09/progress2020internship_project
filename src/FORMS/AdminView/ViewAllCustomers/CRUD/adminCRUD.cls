 
/*------------------------------------------------------------------------
   File        : adminCRUD
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Sat Nov 14 01:29:46 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING src.Classes.BusinessTask.btCustomer FROM PROPATH.
USING src.FORMS.AdminView.ViewAllCustomers.CRUD.DeleteConfirmation FROM PROPATH.
USING src.FORMS.AdminView.ViewAllCustomers.CRUD.UpdateConfirmation FROM PROPATH.

USING OpenEdge.Core.Assert FROM PROPATH.
USING src.Classes.BusinessEntity.beCustomer FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.ViewAllCustomers.CRUD.adminCRUD INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnSaveChanges     AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnGoBack          AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnDeleteCust      AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components         AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE daCustomer         AS daCustomers                      NO-UNDO.
    DEFINE PRIVATE VARIABLE label1             AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCustNum         AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblTerms           AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblSalesRep        AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCreditLimit     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblBalance         AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE textBalance        AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textCreditLimit    AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textTerms          AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textSalesRep       AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textState          AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textPostalCode     AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textPhone          AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textName           AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textFax            AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textEmailAddress   AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textCountry        AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textCity           AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textAddress2       AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textAddress        AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblState           AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPostalCode      AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPhone           AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblFull_name       AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblFax             AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblEmailAddress    AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCountry         AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCity            AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblAddress2        AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblAddress         AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE textUser_name      AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE txtCustNum         AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE btCustomer         AS btCustomer                       NO-UNDO.
    DEFINE PRIVATE VARIABLE deleteConfirmation AS DeleteConfirmation               NO-UNDO.
    DEFINE PRIVATE VARIABLE updateConfirmation AS UpdateConfirmation               NO-UNDO.
    DEFINE PRIVATE VARIABLE beCustomer         AS beCustomer                       NO-UNDO.
    
    
    CONSTRUCTOR PUBLIC adminCRUD (INPUT customernumber AS INTEGER):
        
        InitializeComponent().
        
        DATASET dsCustomer:EMPTY-DATASET ().
        
        btCustomer = NEW btCustomer().
        beCustomer = NEW beCustomer().
        beCustomer:getCustomer(OUTPUT dataset dsCustomer BY-REFERENCE,customernumber).
        FIND FIRST ttCustomer EXCLUSIVE-LOCK NO-ERROR.
        
        
        ASSIGN
            txtCustNum:text       = STRING(ttCustomer.CustNum)
            textBalance:text      = STRING(ttCustomer.Balance)
            textCreditLimit:text  = STRING(ttCustomer.CreditLimit)
            textName:text         = ttCustomer.Name          
            textAddress:text      = ttCustomer.Address       
            textAddress2:text     = ttCustomer.Address2      
            textCity:text         = ttCustomer.City          
            textState:text        = ttCustomer.State         
            textCountry:text      = ttCustomer.Country       
            textEmailAddress:text = ttCustomer.EmailAddress  
            textPostalCode:text   = ttCustomer.PostalCode    
            textPhone:text        = ttCustomer.Phone         
            textUser_name:text    = ttCustomer.User_Name 
            textFax:text          = ttCustomer.fax
            textSalesRep:text     = ttCustomer.SalesRep
            textTerms:text        = ttCustomer.Terms.
              
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID adminCRUD_Load( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnDeleteCust_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        
        deleteConfirmation = NEW DeleteConfirmation(ttCustomer.CustNum).
        deleteConfirmation:Show().
		
        
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnGoBack_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
		
        DELETE OBJECT THIS-OBJECT.
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnSaveChanges_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        assert:notEmpty(textName:text).
        assert:notEmpty(textAddress:text).
        assert:notEmpty(textcity:text).
        assert:notEmpty(textCountry:text).
        assert:notEmpty(textPhone:text).
        assert:notEmpty(textState:text).
        
        ASSIGN  
            ttcustomer.Address      = textAddress:text
            ttcustomer.Address2     = textAddress2:text
            ttcustomer.Balance      = DECIMAL(textBalance:text)
            ttcustomer.City         = textCity:text
            ttcustomer.Country      = textCountry:text
            ttcustomer.CreditLimit  = DECIMAL(textCreditLimit:text)
            ttcustomer.EmailAddress = textEmailAddress:text
            ttcustomer.Fax          = textFax:text
            ttcustomer.Name         = textName:text
            ttcustomer.Phone        = textPhone:text
            ttcustomer.PostalCode   = textPostalCode:text
            ttcustomer.SalesRep     = textSalesRep:text
            ttcustomer.State        = textState:text
            ttcustomer.Terms        = textTerms:text.
        

        updateConfirmation = NEW UpdateConfirmation(INPUT dataset dsCustomer BY-REFERENCE).
        updateConfirmation:show().

       




    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
            
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
            
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:lblState = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblPostalCode = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textState = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textPostalCode = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblPhone = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textPhone = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblFax = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textFax = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblEmailAddress = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textEmailAddress = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblCountry = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textCountry = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblCity = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textCity = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblAddress2 = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textCreditLimit = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblAddress = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textBalance = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblFull_name = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textAddress2 = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textAddress = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textName = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblBalance = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblCreditLimit = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblSalesRep = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblTerms = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textSalesRep = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textTerms = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:txtCustNum = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblCustNum = NEW System.Windows.Forms.Label().
        THIS-OBJECT:btnSaveChanges = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnDeleteCust = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnGoBack = NEW System.Windows.Forms.Button().
        THIS-OBJECT:label1 = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textUser_name = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* lblState */
        /*  */
        THIS-OBJECT:lblState:Location = NEW System.Drawing.Point(50, 366).
        THIS-OBJECT:lblState:Name = "lblState".
        THIS-OBJECT:lblState:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblState:TabIndex = 53.
        THIS-OBJECT:lblState:Text = "State:".
        THIS-OBJECT:lblState:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblPostalCode */
        /*  */
        THIS-OBJECT:lblPostalCode:Location = NEW System.Drawing.Point(50, 340).
        THIS-OBJECT:lblPostalCode:Name = "lblPostalCode".
        THIS-OBJECT:lblPostalCode:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblPostalCode:TabIndex = 52.
        THIS-OBJECT:lblPostalCode:Text = "Postal Code:".
        THIS-OBJECT:lblPostalCode:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textState */
        /*  */
        THIS-OBJECT:textState:Location = NEW System.Drawing.Point(239, 366).
        THIS-OBJECT:textState:Name = "textState".
        THIS-OBJECT:textState:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textState:TabIndex = 51.
        /*  */
        /* textPostalCode */
        /*  */
        THIS-OBJECT:textPostalCode:Location = NEW System.Drawing.Point(239, 340).
        THIS-OBJECT:textPostalCode:Name = "textPostalCode".
        THIS-OBJECT:textPostalCode:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textPostalCode:TabIndex = 50.
        /*  */
        /* lblPhone */
        /*  */
        THIS-OBJECT:lblPhone:Location = NEW System.Drawing.Point(50, 314).
        THIS-OBJECT:lblPhone:Name = "lblPhone".
        THIS-OBJECT:lblPhone:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblPhone:TabIndex = 49.
        THIS-OBJECT:lblPhone:Text = "Phone number:".
        THIS-OBJECT:lblPhone:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textPhone */
        /*  */
        THIS-OBJECT:textPhone:Location = NEW System.Drawing.Point(239, 314).
        THIS-OBJECT:textPhone:Name = "textPhone".
        THIS-OBJECT:textPhone:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textPhone:TabIndex = 48.
        /*  */
        /* lblFax */
        /*  */
        THIS-OBJECT:lblFax:Location = NEW System.Drawing.Point(50, 288).
        THIS-OBJECT:lblFax:Name = "lblFax".
        THIS-OBJECT:lblFax:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblFax:TabIndex = 47.
        THIS-OBJECT:lblFax:Text = "Fax".
        THIS-OBJECT:lblFax:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textFax */
        /*  */
        THIS-OBJECT:textFax:Location = NEW System.Drawing.Point(239, 288).
        THIS-OBJECT:textFax:Name = "textFax".
        THIS-OBJECT:textFax:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textFax:TabIndex = 46.
        /*  */
        /* lblEmailAddress */
        /*  */
        THIS-OBJECT:lblEmailAddress:Location = NEW System.Drawing.Point(50, 262).
        THIS-OBJECT:lblEmailAddress:Name = "lblEmailAddress".
        THIS-OBJECT:lblEmailAddress:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblEmailAddress:TabIndex = 45.
        THIS-OBJECT:lblEmailAddress:Text = "Email Address:".
        THIS-OBJECT:lblEmailAddress:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textEmailAddress */
        /*  */
        THIS-OBJECT:textEmailAddress:Location = NEW System.Drawing.Point(239, 262).
        THIS-OBJECT:textEmailAddress:Name = "textEmailAddress".
        THIS-OBJECT:textEmailAddress:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textEmailAddress:TabIndex = 44.
        /*  */
        /* lblCountry */
        /*  */
        THIS-OBJECT:lblCountry:Location = NEW System.Drawing.Point(50, 236).
        THIS-OBJECT:lblCountry:Name = "lblCountry".
        THIS-OBJECT:lblCountry:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblCountry:TabIndex = 43.
        THIS-OBJECT:lblCountry:Text = "Country".
        THIS-OBJECT:lblCountry:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textCountry */
        /*  */
        THIS-OBJECT:textCountry:Location = NEW System.Drawing.Point(239, 236).
        THIS-OBJECT:textCountry:Name = "textCountry".
        THIS-OBJECT:textCountry:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textCountry:TabIndex = 42.
        /*  */
        /* lblCity */
        /*  */
        THIS-OBJECT:lblCity:Location = NEW System.Drawing.Point(50, 210).
        THIS-OBJECT:lblCity:Name = "lblCity".
        THIS-OBJECT:lblCity:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblCity:TabIndex = 41.
        THIS-OBJECT:lblCity:Text = "City:".
        THIS-OBJECT:lblCity:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textCity */
        /*  */
        THIS-OBJECT:textCity:Location = NEW System.Drawing.Point(239, 210).
        THIS-OBJECT:textCity:Name = "textCity".
        THIS-OBJECT:textCity:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textCity:TabIndex = 40.
        /*  */
        /* lblAddress2 */
        /*  */
        THIS-OBJECT:lblAddress2:Location = NEW System.Drawing.Point(50, 132).
        THIS-OBJECT:lblAddress2:Name = "lblAddress2".
        THIS-OBJECT:lblAddress2:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblAddress2:TabIndex = 39.
        THIS-OBJECT:lblAddress2:Text = "Address2:".
        THIS-OBJECT:lblAddress2:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textCreditLimit */
        /*  */
        THIS-OBJECT:textCreditLimit:AutoCompleteMode = System.Windows.Forms.AutoCompleteMode:Append.
        THIS-OBJECT:textCreditLimit:AutoCompleteSource = System.Windows.Forms.AutoCompleteSource:CustomSource.
        THIS-OBJECT:textCreditLimit:Location = NEW System.Drawing.Point(239, 184).
        THIS-OBJECT:textCreditLimit:Name = "textCreditLimit".
        THIS-OBJECT:textCreditLimit:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textCreditLimit:TabIndex = 38.
        /*  */
        /* lblAddress */
        /*  */
        THIS-OBJECT:lblAddress:Location = NEW System.Drawing.Point(50, 106).
        THIS-OBJECT:lblAddress:Name = "lblAddress".
        THIS-OBJECT:lblAddress:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblAddress:TabIndex = 37.
        THIS-OBJECT:lblAddress:Text = "Address:".
        THIS-OBJECT:lblAddress:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblAddress:Click:Subscribe(THIS-OBJECT:lblAddress_Click).
        /*  */
        /* textBalance */
        /*  */
        THIS-OBJECT:textBalance:Location = NEW System.Drawing.Point(239, 158).
        THIS-OBJECT:textBalance:Name = "textBalance".
        THIS-OBJECT:textBalance:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textBalance:TabIndex = 36.
        /*  */
        /* lblFull_name */
        /*  */
        THIS-OBJECT:lblFull_name:Location = NEW System.Drawing.Point(50, 80).
        THIS-OBJECT:lblFull_name:Name = "lblFull_name".
        THIS-OBJECT:lblFull_name:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblFull_name:TabIndex = 32.
        THIS-OBJECT:lblFull_name:Text = "Full Name:".
        THIS-OBJECT:lblFull_name:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblFull_name:Click:Subscribe(THIS-OBJECT:lblFull_name_Click).
        /*  */
        /* textAddress2 */
        /*  */
        THIS-OBJECT:textAddress2:Location = NEW System.Drawing.Point(239, 132).
        THIS-OBJECT:textAddress2:Name = "textAddress2".
        THIS-OBJECT:textAddress2:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textAddress2:TabIndex = 31.
        /*  */
        /* textAddress */
        /*  */
        THIS-OBJECT:textAddress:Location = NEW System.Drawing.Point(239, 106).
        THIS-OBJECT:textAddress:Name = "textAddress".
        THIS-OBJECT:textAddress:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textAddress:TabIndex = 30.
        /*  */
        /* textName */
        /*  */
        THIS-OBJECT:textName:Location = NEW System.Drawing.Point(239, 80).
        THIS-OBJECT:textName:Name = "textName".
        THIS-OBJECT:textName:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textName:TabIndex = 28.
        /*  */
        /* lblBalance */
        /*  */
        THIS-OBJECT:lblBalance:Location = NEW System.Drawing.Point(50, 158).
        THIS-OBJECT:lblBalance:Name = "lblBalance".
        THIS-OBJECT:lblBalance:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblBalance:TabIndex = 54.
        THIS-OBJECT:lblBalance:Text = "Balance:".
        THIS-OBJECT:lblBalance:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblBalance:Click:Subscribe(THIS-OBJECT:lblBalance_Click).
        /*  */
        /* lblCreditLimit */
        /*  */
        THIS-OBJECT:lblCreditLimit:Location = NEW System.Drawing.Point(50, 184).
        THIS-OBJECT:lblCreditLimit:Name = "lblCreditLimit".
        THIS-OBJECT:lblCreditLimit:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblCreditLimit:TabIndex = 55.
        THIS-OBJECT:lblCreditLimit:Text = "Credit Limit:".
        THIS-OBJECT:lblCreditLimit:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblSalesRep */
        /*  */
        THIS-OBJECT:lblSalesRep:Location = NEW System.Drawing.Point(50, 392).
        THIS-OBJECT:lblSalesRep:Name = "lblSalesRep".
        THIS-OBJECT:lblSalesRep:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblSalesRep:TabIndex = 56.
        THIS-OBJECT:lblSalesRep:Text = "Sales Rep:".
        THIS-OBJECT:lblSalesRep:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblTerms */
        /*  */
        THIS-OBJECT:lblTerms:Location = NEW System.Drawing.Point(50, 418).
        THIS-OBJECT:lblTerms:Name = "lblTerms".
        THIS-OBJECT:lblTerms:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblTerms:TabIndex = 57.
        THIS-OBJECT:lblTerms:Text = "Terms:".
        THIS-OBJECT:lblTerms:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textSalesRep */
        /*  */
        THIS-OBJECT:textSalesRep:Location = NEW System.Drawing.Point(239, 392).
        THIS-OBJECT:textSalesRep:Name = "textSalesRep".
        THIS-OBJECT:textSalesRep:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textSalesRep:TabIndex = 58.
        /*  */
        /* textTerms */
        /*  */
        THIS-OBJECT:textTerms:Location = NEW System.Drawing.Point(239, 418).
        THIS-OBJECT:textTerms:Name = "textTerms".
        THIS-OBJECT:textTerms:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textTerms:TabIndex = 59.
        /*  */
        /* txtCustNum */
        /*  */
        THIS-OBJECT:txtCustNum:Location = NEW System.Drawing.Point(239, 28).
        THIS-OBJECT:txtCustNum:Name = "txtCustNum".
        THIS-OBJECT:txtCustNum:ReadOnly = TRUE.
        THIS-OBJECT:txtCustNum:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:txtCustNum:TabIndex = 60.
        /*  */
        /* lblCustNum */
        /*  */
        THIS-OBJECT:lblCustNum:Location = NEW System.Drawing.Point(50, 28).
        THIS-OBJECT:lblCustNum:Name = "lblCustNum".
        THIS-OBJECT:lblCustNum:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblCustNum:TabIndex = 61.
        THIS-OBJECT:lblCustNum:Text = "Customer Number:".
        THIS-OBJECT:lblCustNum:UseCompatibleTextRendering = TRUE.
        /*  */
        /* btnSaveChanges */
        /*  */
        THIS-OBJECT:btnSaveChanges:Location = NEW System.Drawing.Point(144, 478).
        THIS-OBJECT:btnSaveChanges:Name = "btnSaveChanges".
        THIS-OBJECT:btnSaveChanges:Size = NEW System.Drawing.Size(160, 35).
        THIS-OBJECT:btnSaveChanges:TabIndex = 62.
        THIS-OBJECT:btnSaveChanges:Text = "Save Changes".
        THIS-OBJECT:btnSaveChanges:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnSaveChanges:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnSaveChanges:Click:Subscribe(THIS-OBJECT:btnSaveChanges_Click).
        /*  */
        /* btnDeleteCust */
        /*  */
        THIS-OBJECT:btnDeleteCust:Location = NEW System.Drawing.Point(144, 519).
        THIS-OBJECT:btnDeleteCust:Name = "btnDeleteCust".
        THIS-OBJECT:btnDeleteCust:Size = NEW System.Drawing.Size(160, 35).
        THIS-OBJECT:btnDeleteCust:TabIndex = 63.
        THIS-OBJECT:btnDeleteCust:Text = "Delte Customer".
        THIS-OBJECT:btnDeleteCust:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnDeleteCust:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnDeleteCust:Click:Subscribe(THIS-OBJECT:btnDeleteCust_Click).
        /*  */
        /* btnGoBack */
        /*  */
        THIS-OBJECT:btnGoBack:Location = NEW System.Drawing.Point(144, 560).
        THIS-OBJECT:btnGoBack:Name = "btnGoBack".
        THIS-OBJECT:btnGoBack:Size = NEW System.Drawing.Size(160, 35).
        THIS-OBJECT:btnGoBack:TabIndex = 64.
        THIS-OBJECT:btnGoBack:Text = "Go Back".
        THIS-OBJECT:btnGoBack:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnGoBack:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnGoBack:Click:Subscribe(THIS-OBJECT:btnGoBack_Click).
        /*  */
        /* label1 */
        /*  */
        THIS-OBJECT:label1:Location = NEW System.Drawing.Point(50, 54).
        THIS-OBJECT:label1:Name = "label1".
        THIS-OBJECT:label1:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:label1:TabIndex = 66.
        THIS-OBJECT:label1:Text = "Username:".
        THIS-OBJECT:label1:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textUser_name */
        /*  */
        THIS-OBJECT:textUser_name:Location = NEW System.Drawing.Point(239, 54).
        THIS-OBJECT:textUser_name:Name = "textUser_name".
        THIS-OBJECT:textUser_name:ReadOnly = TRUE.
        THIS-OBJECT:textUser_name:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textUser_name:TabIndex = 65.
        /*  */
        /* adminCRUD */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(449, 643).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:label1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textUser_name).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnGoBack).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnDeleteCust).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnSaveChanges).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCustNum).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtCustNum).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textTerms).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textSalesRep).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblTerms).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblSalesRep).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCreditLimit).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblBalance).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblState).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPostalCode).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textState).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textPostalCode).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPhone).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textPhone).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblFax).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textFax).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblEmailAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textEmailAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCountry).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textCountry).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCity).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textCity).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblAddress2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textCreditLimit).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textBalance).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblFull_name).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textAddress2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textName).
        THIS-OBJECT:Name = "adminCRUD".
        THIS-OBJECT:Text = "adminCRUD".
        THIS-OBJECT:Load:Subscribe(THIS-OBJECT:adminCRUD_Load).
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID lblAddress_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID lblBalance_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID lblFull_name_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC adminCRUD ( ):

    END DESTRUCTOR.

END CLASS.