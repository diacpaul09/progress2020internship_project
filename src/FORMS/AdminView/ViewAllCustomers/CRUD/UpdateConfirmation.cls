 
/*------------------------------------------------------------------------
   File        : UpdateConfirmation
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Sat Nov 14 19:45:35 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.BusinessTask.btCustomer FROM PROPATH.
USING src.Classes.BusinessEntity.beCustomer FROM PROPATH.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.ViewAllCustomers.CRUD.UpdateConfirmation INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnYes           AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnNo            AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components       AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE label1           AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE custnum          AS INTEGER.
    DEFINE PRIVATE VARIABLE btCustomer       AS btCustomer.
    DEFINE PRIVATE VARIABLE UPfullName       AS CHARACTER.   
    DEFINE PRIVATE VARIABLE UPaddress        AS CHARACTER.    
    DEFINE PRIVATE VARIABLE UPaddress2       AS CHARACTER.    
    DEFINE PRIVATE VARIABLE UPbalance        AS DECIMAL.    
    DEFINE PRIVATE VARIABLE UPcreditLimit    AS DECIMAL.   
    DEFINE PRIVATE VARIABLE UPcity           AS CHARACTER.   
    DEFINE PRIVATE VARIABLE UPcountry        AS CHARACTER.     
    DEFINE PRIVATE VARIABLE UPemailAddress   AS CHARACTER.
    DEFINE PRIVATE VARIABLE UPfax            AS CHARACTER.         
    DEFINE PRIVATE VARIABLE UPphoneNumber    AS CHARACTER. 
    DEFINE PRIVATE VARIABLE UPpostalCode     AS CHARACTER.  
    DEFINE PRIVATE VARIABLE UPstate          AS CHARACTER.       
    DEFINE PRIVATE VARIABLE UPsalesRep       AS CHARACTER.    
    DEFINE PRIVATE VARIABLE UPterms          AS CHARACTER.    
    DEFINE PRIVATE VARIABLE beCustomer       AS beCustomer . 
    DEFINE PRIVATE VARIABLE dsCustomerHandle AS HANDLE                           NO-UNDO.
    DEFINE PRIVATE VARIABLE customernumber   AS INTEGER                          NO-UNDO.
    DEFINE PRIVATE VARIABLE daCustomer       AS daCustomers                      NO-UNDO.
    
    
    

    CONSTRUCTOR PUBLIC UpdateConfirmation (INPUT dataset dsCustomer ):            
        
        InitializeComponent().
        
        
        
        dsCustomerHandle = DATASET dsCustomer:handle.
        beCustomer = NEW beCustomer().
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnNo_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnYes_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        
        
        
        
        beCustomer:UpdateCustomer(INPUT-OUTPUT DATASET-handle dsCustomerHandle BY-REFERENCE).


        MESSAGE "Customer has been succesfully updated"
            VIEW-AS ALERT-BOX.

        DELETE OBJECT THIS-OBJECT.
        RETURN.

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:label1 = NEW System.Windows.Forms.Label().
        THIS-OBJECT:btnNo = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnYes = NEW System.Windows.Forms.Button().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* label1 */
        /*  */
        THIS-OBJECT:label1:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:label1:Location = NEW System.Drawing.Point(38, 13).
        THIS-OBJECT:label1:Name = "label1".
        THIS-OBJECT:label1:Size = NEW System.Drawing.Size(217, 64).
        THIS-OBJECT:label1:TabIndex = 5.
        THIS-OBJECT:label1:Text = "Are you sure you want to update this customer?".
        THIS-OBJECT:label1:TextAlign = System.Drawing.ContentAlignment:MiddleCenter.
        THIS-OBJECT:label1:UseCompatibleTextRendering = TRUE.
        /*  */
        /* btnNo */
        /*  */
        THIS-OBJECT:btnNo:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(11.25), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:btnNo:Location = NEW System.Drawing.Point(168, 80).
        THIS-OBJECT:btnNo:Name = "btnNo".
        THIS-OBJECT:btnNo:Size = NEW System.Drawing.Size(112, 36).
        THIS-OBJECT:btnNo:TabIndex = 4.
        THIS-OBJECT:btnNo:Text = "No".
        THIS-OBJECT:btnNo:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnNo:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnNo:Click:Subscribe(THIS-OBJECT:btnNo_Click).
        /*  */
        /* btnYes */
        /*  */
        THIS-OBJECT:btnYes:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(11.25), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:btnYes:Location = NEW System.Drawing.Point(13, 80).
        THIS-OBJECT:btnYes:Name = "btnYes".
        THIS-OBJECT:btnYes:Size = NEW System.Drawing.Size(112, 36).
        THIS-OBJECT:btnYes:TabIndex = 3.
        THIS-OBJECT:btnYes:Text = "Yes".
        THIS-OBJECT:btnYes:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnYes:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnYes:Click:Subscribe(THIS-OBJECT:btnYes_Click).
        /*  */
        /* UpdateConfirmation */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(292, 128).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:label1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnNo).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnYes).
        THIS-OBJECT:Name = "UpdateConfirmation".
        THIS-OBJECT:Text = "UpdateConfirmation".
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC UpdateConfirmation ( ):

    END DESTRUCTOR.

END CLASS.