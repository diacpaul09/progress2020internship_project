 
/*------------------------------------------------------------------------
   File        : ViewAllCustomers
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Thu Nov 12 12:16:11 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING src.FORMS.AdminView.ViewAllCustomers.ViewAllCustomers FROM PROPATH.
USING src.FORMS.AdminView.ViewAllCustomers.CRUD.adminCRUD FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.ViewAllCustomers.ViewAllCustomers INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnGoBack                             AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE commentsDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE cityDataGridViewTextBoxColumn         AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE bsCustomers                           AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE balanceDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE addressDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE address2DataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE components                            AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE passwordDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn9            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn8            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn7            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn6            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn5            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn4            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn3            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn2            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn18           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn17           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn16           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn15           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn14           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn13           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn12           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn11           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn10           AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridViewTextBoxColumn1            AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE termsDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE stateDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE salesRepDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE postalCodeDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE phoneDataGridViewTextBoxColumn        AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE nameDataGridViewTextBoxColumn         AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE faxDataGridViewTextBoxColumn          AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE emailAddressDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE discountDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                         AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE custNumDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE creditLimitDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE countryDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE contactDataGridViewTextBoxColumn      AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE daCustomers                           AS daCustomers.
    DEFINE PRIVATE VARIABLE userNameDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE ViewAllCustomers                      AS ViewAllCustomers.
    DEFINE PRIVATE VARIABLE adminCRUD                             AS adminCRUD                                      NO-UNDO.
    CONSTRUCTOR PUBLIC ViewAllCustomers (  ):
        
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        daCustomers = NEW daCustomers().
        daCustomers:FetchData(OUTPUT dataset dsCustomer BY-REFERENCE).
        DATASET dsCustomer:TOP-NAV-QUERY:query-open (). 
        bsCustomers:handle = DATASET dsCustomer:HANDLE.
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnGoBack_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_CellContentClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.DataGridViewCellEventArgs ):
		
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_MouseClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.MouseEventArgs ):
		
        adminCRUD = NEW adminCRUD(ttCustomer.CustNum).
        adminCRUD:Show().

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc1 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc1 = NEW Progress.Data.TableDesc("ttCustomer").
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:dataGridViewTextBoxColumn1 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn2 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn3 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn4 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn5 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn6 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn7 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn8 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn9 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn10 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn11 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn12 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn13 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:passwordDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn14 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn15 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn16 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn17 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:dataGridViewTextBoxColumn18 = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:userNameDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:bsCustomers = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:btnGoBack = NEW System.Windows.Forms.Button().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:bsCustomers, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS System.Windows.Forms.DataGridViewColumn EXTENT 20 NO-UNDO.
        arrayvar0[1] = THIS-OBJECT:dataGridViewTextBoxColumn1.
        arrayvar0[2] = THIS-OBJECT:dataGridViewTextBoxColumn2.
        arrayvar0[3] = THIS-OBJECT:dataGridViewTextBoxColumn3.
        arrayvar0[4] = THIS-OBJECT:dataGridViewTextBoxColumn4.
        arrayvar0[5] = THIS-OBJECT:dataGridViewTextBoxColumn5.
        arrayvar0[6] = THIS-OBJECT:dataGridViewTextBoxColumn6.
        arrayvar0[7] = THIS-OBJECT:dataGridViewTextBoxColumn7.
        arrayvar0[8] = THIS-OBJECT:dataGridViewTextBoxColumn8.
        arrayvar0[9] = THIS-OBJECT:dataGridViewTextBoxColumn9.
        arrayvar0[10] = THIS-OBJECT:dataGridViewTextBoxColumn10.
        arrayvar0[11] = THIS-OBJECT:dataGridViewTextBoxColumn11.
        arrayvar0[12] = THIS-OBJECT:dataGridViewTextBoxColumn12.
        arrayvar0[13] = THIS-OBJECT:dataGridViewTextBoxColumn13.
        arrayvar0[14] = THIS-OBJECT:passwordDataGridViewTextBoxColumn.
        arrayvar0[15] = THIS-OBJECT:dataGridViewTextBoxColumn14.
        arrayvar0[16] = THIS-OBJECT:dataGridViewTextBoxColumn15.
        arrayvar0[17] = THIS-OBJECT:dataGridViewTextBoxColumn16.
        arrayvar0[18] = THIS-OBJECT:dataGridViewTextBoxColumn17.
        arrayvar0[19] = THIS-OBJECT:dataGridViewTextBoxColumn18.
        arrayvar0[20] = THIS-OBJECT:userNameDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar0).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bsCustomers.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(12, 12).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(1069, 417).
        THIS-OBJECT:dataGridView1:TabIndex = 0.
        THIS-OBJECT:dataGridView1:CellContentClick:Subscribe(THIS-OBJECT:dataGridView1_CellContentClick).
        THIS-OBJECT:dataGridView1:MouseClick:Subscribe(THIS-OBJECT:dataGridView1_MouseClick).
        /*  */
        /* dataGridViewTextBoxColumn1 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn1:DataPropertyName = "Address".
        THIS-OBJECT:dataGridViewTextBoxColumn1:HeaderText = "Address".
        THIS-OBJECT:dataGridViewTextBoxColumn1:Name = "dataGridViewTextBoxColumn1".
        /*  */
        /* dataGridViewTextBoxColumn2 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn2:DataPropertyName = "Address2".
        THIS-OBJECT:dataGridViewTextBoxColumn2:HeaderText = "Address2".
        THIS-OBJECT:dataGridViewTextBoxColumn2:Name = "dataGridViewTextBoxColumn2".
        /*  */
        /* dataGridViewTextBoxColumn3 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn3:DataPropertyName = "Balance".
        THIS-OBJECT:dataGridViewTextBoxColumn3:HeaderText = "Balance".
        THIS-OBJECT:dataGridViewTextBoxColumn3:Name = "dataGridViewTextBoxColumn3".
        /*  */
        /* dataGridViewTextBoxColumn4 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn4:DataPropertyName = "City".
        THIS-OBJECT:dataGridViewTextBoxColumn4:HeaderText = "City".
        THIS-OBJECT:dataGridViewTextBoxColumn4:Name = "dataGridViewTextBoxColumn4".
        /*  */
        /* dataGridViewTextBoxColumn5 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn5:DataPropertyName = "Comments".
        THIS-OBJECT:dataGridViewTextBoxColumn5:HeaderText = "Comments".
        THIS-OBJECT:dataGridViewTextBoxColumn5:Name = "dataGridViewTextBoxColumn5".
        /*  */
        /* dataGridViewTextBoxColumn6 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn6:DataPropertyName = "Contact".
        THIS-OBJECT:dataGridViewTextBoxColumn6:HeaderText = "Contact".
        THIS-OBJECT:dataGridViewTextBoxColumn6:Name = "dataGridViewTextBoxColumn6".
        /*  */
        /* dataGridViewTextBoxColumn7 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn7:DataPropertyName = "Country".
        THIS-OBJECT:dataGridViewTextBoxColumn7:HeaderText = "Country".
        THIS-OBJECT:dataGridViewTextBoxColumn7:Name = "dataGridViewTextBoxColumn7".
        /*  */
        /* dataGridViewTextBoxColumn8 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn8:DataPropertyName = "CreditLimit".
        THIS-OBJECT:dataGridViewTextBoxColumn8:HeaderText = "Credit Limit".
        THIS-OBJECT:dataGridViewTextBoxColumn8:Name = "dataGridViewTextBoxColumn8".
        /*  */
        /* dataGridViewTextBoxColumn9 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn9:DataPropertyName = "CustNum".
        THIS-OBJECT:dataGridViewTextBoxColumn9:HeaderText = "Cust Num".
        THIS-OBJECT:dataGridViewTextBoxColumn9:Name = "dataGridViewTextBoxColumn9".
        /*  */
        /* dataGridViewTextBoxColumn10 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn10:DataPropertyName = "Discount".
        THIS-OBJECT:dataGridViewTextBoxColumn10:HeaderText = "Discount".
        THIS-OBJECT:dataGridViewTextBoxColumn10:Name = "dataGridViewTextBoxColumn10".
        /*  */
        /* dataGridViewTextBoxColumn11 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn11:DataPropertyName = "EmailAddress".
        THIS-OBJECT:dataGridViewTextBoxColumn11:HeaderText = "Email".
        THIS-OBJECT:dataGridViewTextBoxColumn11:Name = "dataGridViewTextBoxColumn11".
        /*  */
        /* dataGridViewTextBoxColumn12 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn12:DataPropertyName = "Fax".
        THIS-OBJECT:dataGridViewTextBoxColumn12:HeaderText = "Fax".
        THIS-OBJECT:dataGridViewTextBoxColumn12:Name = "dataGridViewTextBoxColumn12".
        /*  */
        /* dataGridViewTextBoxColumn13 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn13:DataPropertyName = "Name".
        THIS-OBJECT:dataGridViewTextBoxColumn13:HeaderText = "Name".
        THIS-OBJECT:dataGridViewTextBoxColumn13:Name = "dataGridViewTextBoxColumn13".
        /*  */
        /* passwordDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:passwordDataGridViewTextBoxColumn:DataPropertyName = "Password".
        THIS-OBJECT:passwordDataGridViewTextBoxColumn:HeaderText = "Password".
        THIS-OBJECT:passwordDataGridViewTextBoxColumn:Name = "passwordDataGridViewTextBoxColumn".
        /*  */
        /* dataGridViewTextBoxColumn14 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn14:DataPropertyName = "Phone".
        THIS-OBJECT:dataGridViewTextBoxColumn14:HeaderText = "Phone".
        THIS-OBJECT:dataGridViewTextBoxColumn14:Name = "dataGridViewTextBoxColumn14".
        /*  */
        /* dataGridViewTextBoxColumn15 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn15:DataPropertyName = "PostalCode".
        THIS-OBJECT:dataGridViewTextBoxColumn15:HeaderText = "Postal Code".
        THIS-OBJECT:dataGridViewTextBoxColumn15:Name = "dataGridViewTextBoxColumn15".
        /*  */
        /* dataGridViewTextBoxColumn16 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn16:DataPropertyName = "SalesRep".
        THIS-OBJECT:dataGridViewTextBoxColumn16:HeaderText = "Sales Rep".
        THIS-OBJECT:dataGridViewTextBoxColumn16:Name = "dataGridViewTextBoxColumn16".
        /*  */
        /* dataGridViewTextBoxColumn17 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn17:DataPropertyName = "State".
        THIS-OBJECT:dataGridViewTextBoxColumn17:HeaderText = "State".
        THIS-OBJECT:dataGridViewTextBoxColumn17:Name = "dataGridViewTextBoxColumn17".
        /*  */
        /* dataGridViewTextBoxColumn18 */
        /*  */
        THIS-OBJECT:dataGridViewTextBoxColumn18:DataPropertyName = "Terms".
        THIS-OBJECT:dataGridViewTextBoxColumn18:HeaderText = "Terms".
        THIS-OBJECT:dataGridViewTextBoxColumn18:Name = "dataGridViewTextBoxColumn18".
        /*  */
        /* userNameDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:userNameDataGridViewTextBoxColumn:DataPropertyName = "User_Name".
        THIS-OBJECT:userNameDataGridViewTextBoxColumn:HeaderText = "UserName".
        THIS-OBJECT:userNameDataGridViewTextBoxColumn:Name = "userNameDataGridViewTextBoxColumn".
        /*  */
        /* bsCustomers */
        /*  */
        THIS-OBJECT:bsCustomers:MaxDataGuess = 0.
        THIS-OBJECT:bsCustomers:NoLOBs = FALSE.
        THIS-OBJECT:bsCustomers:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar1 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc1:ChildTables = arrayvar1.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS Progress.Data.ColumnPropDesc EXTENT 20 NO-UNDO.
        arrayvar2[1] = NEW Progress.Data.ColumnPropDesc("Address", "Address", Progress.Data.DataType:CHARACTER).
        arrayvar2[2] = NEW Progress.Data.ColumnPropDesc("Address2", "Address2", Progress.Data.DataType:CHARACTER).
        arrayvar2[3] = NEW Progress.Data.ColumnPropDesc("Balance", "Balance", Progress.Data.DataType:DECIMAL).
        arrayvar2[4] = NEW Progress.Data.ColumnPropDesc("City", "City", Progress.Data.DataType:CHARACTER).
        arrayvar2[5] = NEW Progress.Data.ColumnPropDesc("Comments", "Comments", Progress.Data.DataType:CHARACTER).
        arrayvar2[6] = NEW Progress.Data.ColumnPropDesc("Contact", "Contact", Progress.Data.DataType:CHARACTER).
        arrayvar2[7] = NEW Progress.Data.ColumnPropDesc("Country", "Country", Progress.Data.DataType:CHARACTER).
        arrayvar2[8] = NEW Progress.Data.ColumnPropDesc("CreditLimit", "Credit Limit", Progress.Data.DataType:DECIMAL).
        arrayvar2[9] = NEW Progress.Data.ColumnPropDesc("CustNum", "Cust Num", Progress.Data.DataType:INTEGER).
        arrayvar2[10] = NEW Progress.Data.ColumnPropDesc("Discount", "Discount", Progress.Data.DataType:INTEGER).
        arrayvar2[11] = NEW Progress.Data.ColumnPropDesc("EmailAddress", "Email", Progress.Data.DataType:CHARACTER).
        arrayvar2[12] = NEW Progress.Data.ColumnPropDesc("Fax", "Fax", Progress.Data.DataType:CHARACTER).
        arrayvar2[13] = NEW Progress.Data.ColumnPropDesc("Name", "Name", Progress.Data.DataType:CHARACTER).
        arrayvar2[14] = NEW Progress.Data.ColumnPropDesc("Password", "Password", Progress.Data.DataType:CHARACTER).
        arrayvar2[15] = NEW Progress.Data.ColumnPropDesc("Phone", "Phone", Progress.Data.DataType:CHARACTER).
        arrayvar2[16] = NEW Progress.Data.ColumnPropDesc("PostalCode", "Postal Code", Progress.Data.DataType:CHARACTER).
        arrayvar2[17] = NEW Progress.Data.ColumnPropDesc("SalesRep", "Sales Rep", Progress.Data.DataType:CHARACTER).
        arrayvar2[18] = NEW Progress.Data.ColumnPropDesc("State", "State", Progress.Data.DataType:CHARACTER).
        arrayvar2[19] = NEW Progress.Data.ColumnPropDesc("Terms", "Terms", Progress.Data.DataType:CHARACTER).
        arrayvar2[20] = NEW Progress.Data.ColumnPropDesc("User_Name", "UserName", Progress.Data.DataType:CHARACTER).
        tableDesc1:Columns = arrayvar2.
        THIS-OBJECT:bsCustomers:TableSchema = tableDesc1.
        /*  */
        /* btnGoBack */
        /*  */
        THIS-OBJECT:btnGoBack:Location = NEW System.Drawing.Point(12, 435).
        THIS-OBJECT:btnGoBack:Name = "btnGoBack".
        THIS-OBJECT:btnGoBack:Size = NEW System.Drawing.Size(1069, 71).
        THIS-OBJECT:btnGoBack:TabIndex = 1.
        THIS-OBJECT:btnGoBack:Text = "Go Back".
        THIS-OBJECT:btnGoBack:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnGoBack:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnGoBack:Click:Subscribe(THIS-OBJECT:btnGoBack_Click).
        /*  */
        /* ViewAllCustomers */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(1093, 520).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnGoBack).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "ViewAllCustomers".
        THIS-OBJECT:Text = "ViewAllCustomers".
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:bsCustomers, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC ViewAllCustomers ( ):

    END DESTRUCTOR.

END CLASS.