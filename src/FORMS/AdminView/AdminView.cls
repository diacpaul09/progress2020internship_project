 
/*------------------------------------------------------------------------
   File        : AdminView
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Thu Nov 12 12:13:29 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.FORMS.AdminView.ViewAllCustomers.ViewAllCustomers FROM PROPATH.
USING src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewCustomers.ViewCustomers FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.AdminView.AdminView INHERITS Form: 
    
    DEFINE PRIVATE VARIABLE btnClose                 AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnViewAllCustomers      AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnViewOrdersOfCustomers AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components               AS System.ComponentModel.IContainer NO-UNDO.

    CONSTRUCTOR PUBLIC AdminView (  ):
        
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnViewAllCustomers_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DEFINE VARIABLE viewAllCustomersForm AS ViewAllCustomers.
        viewAllCustomersForm = NEW ViewAllCustomers().
        viewAllCustomersForm:Show().

    END METHOD.

	
	

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnViewOrdersOfCustomers_Click_1( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DEFINE VARIABLE viewCustomers AS ViewCustomers.
        viewCustomers = NEW ViewCustomers().
        viewCustomers:Show().

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:btnViewAllCustomers = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnViewOrdersOfCustomers = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* btnViewAllCustomers */
        /*  */
        THIS-OBJECT:btnViewAllCustomers:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(9.75), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:btnViewAllCustomers:Location = NEW System.Drawing.Point(32, 62).
        THIS-OBJECT:btnViewAllCustomers:Name = "btnViewAllCustomers".
        THIS-OBJECT:btnViewAllCustomers:Size = NEW System.Drawing.Size(266, 74).
        THIS-OBJECT:btnViewAllCustomers:TabIndex = 0.
        THIS-OBJECT:btnViewAllCustomers:Text = "View all Customers".
        THIS-OBJECT:btnViewAllCustomers:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnViewAllCustomers:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnViewAllCustomers:Click:Subscribe(THIS-OBJECT:btnViewAllCustomers_Click).
        /*  */
        /* btnViewOrdersOfCustomers */
        /*  */
        THIS-OBJECT:btnViewOrdersOfCustomers:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(9.75), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:btnViewOrdersOfCustomers:Location = NEW System.Drawing.Point(32, 142).
        THIS-OBJECT:btnViewOrdersOfCustomers:Name = "btnViewOrdersOfCustomers".
        THIS-OBJECT:btnViewOrdersOfCustomers:Size = NEW System.Drawing.Size(266, 74).
        THIS-OBJECT:btnViewOrdersOfCustomers:TabIndex = 1.
        THIS-OBJECT:btnViewOrdersOfCustomers:Text = "View Orders Of Customers".
        THIS-OBJECT:btnViewOrdersOfCustomers:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnViewOrdersOfCustomers:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnViewOrdersOfCustomers:Click:Subscribe(THIS-OBJECT:btnViewOrdersOfCustomers_Click_1).
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(9.75), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(32, 461).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(266, 74).
        THIS-OBJECT:btnClose:TabIndex = 2.
        THIS-OBJECT:btnClose:Text = "Close this view".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* AdminView */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(331, 565).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnViewOrdersOfCustomers).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnViewAllCustomers).
        THIS-OBJECT:Name = "AdminView".
        THIS-OBJECT:Text = "AdminView".
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC AdminView ( ):

    END DESTRUCTOR.

END CLASS.