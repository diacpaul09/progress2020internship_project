 
/*------------------------------------------------------------------------
   File        : CustomerView
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Sun Nov 15 15:06:16 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING src.Classes.BusinessEntity.beCustomer FROM PROPATH.
USING src.FORMS.AdminView.ViewOrdersOfSelectedCustomer.ViewOrdersOfCustomers.ViewOrders FROM PROPATH.
USING src.FORMS.CustomerView.UpdateForm.UpdateForm FROM PROPATH.
USING src.FORMS.CustomerView.CreateANewOrder.CreateANewOrder FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.CustomerView.CustomerView INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnLogOut             AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnCrud               AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnCreateNewOrder     AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnShowPreviousOrders AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components            AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE lblName               AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE custnum               AS INTEGER.
    DEFINE PRIVATE VARIABLE daCustomer            AS daCustomers.
    DEFINE PRIVATE VARIABLE txtName               AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE viewOrders            AS ViewOrders                       NO-UNDO.
    DEFINE PRIVATE VARIABLE updateView            AS UpdateForm                       NO-UNDO.
    DEFINE PRIVATE VARIABLE beCustomer            AS beCustomer                       NO-UNDO.
    DEFINE PRIVATE VARIABLE createNewOrder        AS CreateANewOrder                  NO-UNDO.
    

    CONSTRUCTOR PUBLIC CustomerView (INPUT customernumber AS INTEGER):
        
        InitializeComponent().
        
        custnum =customernumber.
        beCustomer = NEW beCustomer().
        
        beCustomer:getCustomer(OUTPUT dataset dsCustomer BY-REFERENCE, custnum).
        
        FIND FIRST ttCustomer  EXCLUSIVE-LOCK NO-ERROR.
        
        txtName:text = ttCustomer.name.
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnCreateNewOrder_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        createNewOrder = NEW CreateANewOrder(custnum).
        createNewORder:show().

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnCrud_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        updateView = NEW UpdateForm(custnum).
        updateView:show().
        
        

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnLogOut_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnShowPreviousOrders_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        viewOrders = NEW ViewOrders(ttCustomer.CustNum,ttCustomer.Name).
        viewOrders:Show().
        

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:btnShowPreviousOrders = NEW System.Windows.Forms.Button().
        THIS-OBJECT:lblName = NEW System.Windows.Forms.Label().
        THIS-OBJECT:txtName = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:btnCreateNewOrder = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnCrud = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnLogOut = NEW System.Windows.Forms.Button().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* btnShowPreviousOrders */
        /*  */
        THIS-OBJECT:btnShowPreviousOrders:Location = NEW System.Drawing.Point(12, 96).
        THIS-OBJECT:btnShowPreviousOrders:Name = "btnShowPreviousOrders".
        THIS-OBJECT:btnShowPreviousOrders:Size = NEW System.Drawing.Size(411, 98).
        THIS-OBJECT:btnShowPreviousOrders:TabIndex = 0.
        THIS-OBJECT:btnShowPreviousOrders:Text = "Show my previous orders".
        THIS-OBJECT:btnShowPreviousOrders:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnShowPreviousOrders:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnShowPreviousOrders:Click:Subscribe(THIS-OBJECT:btnShowPreviousOrders_Click).
        /*  */
        /* lblName */
        /*  */
        THIS-OBJECT:lblName:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:lblName:Location = NEW System.Drawing.Point(12, 23).
        THIS-OBJECT:lblName:Name = "lblName".
        THIS-OBJECT:lblName:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblName:TabIndex = 1.
        THIS-OBJECT:lblName:Text = "Hello".
        THIS-OBJECT:lblName:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblName:Click:Subscribe(THIS-OBJECT:label1_Click).
        /*  */
        /* txtName */
        /*  */
        THIS-OBJECT:txtName:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:txtName:Location = NEW System.Drawing.Point(62, 20).
        THIS-OBJECT:txtName:Name = "txtName".
        THIS-OBJECT:txtName:ReadOnly = TRUE.
        THIS-OBJECT:txtName:Size = NEW System.Drawing.Size(276, 26).
        THIS-OBJECT:txtName:TabIndex = 2.
        /*  */
        /* btnCreateNewOrder */
        /*  */
        THIS-OBJECT:btnCreateNewOrder:Location = NEW System.Drawing.Point(12, 200).
        THIS-OBJECT:btnCreateNewOrder:Name = "btnCreateNewOrder".
        THIS-OBJECT:btnCreateNewOrder:Size = NEW System.Drawing.Size(411, 98).
        THIS-OBJECT:btnCreateNewOrder:TabIndex = 3.
        THIS-OBJECT:btnCreateNewOrder:Text = "Create a new Order".
        THIS-OBJECT:btnCreateNewOrder:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnCreateNewOrder:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnCreateNewOrder:Click:Subscribe(THIS-OBJECT:btnCreateNewOrder_Click).
        /*  */
        /* btnCrud */
        /*  */
        THIS-OBJECT:btnCrud:Location = NEW System.Drawing.Point(12, 308).
        THIS-OBJECT:btnCrud:Name = "btnCrud".
        THIS-OBJECT:btnCrud:Size = NEW System.Drawing.Size(411, 98).
        THIS-OBJECT:btnCrud:TabIndex = 4.
        THIS-OBJECT:btnCrud:Text = "Update your account details".
        THIS-OBJECT:btnCrud:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnCrud:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnCrud:Click:Subscribe(THIS-OBJECT:btnCrud_Click).
        /*  */
        /* btnLogOut */
        /*  */
        THIS-OBJECT:btnLogOut:Location = NEW System.Drawing.Point(149, 657).
        THIS-OBJECT:btnLogOut:Name = "btnLogOut".
        THIS-OBJECT:btnLogOut:Size = NEW System.Drawing.Size(137, 45).
        THIS-OBJECT:btnLogOut:TabIndex = 5.
        THIS-OBJECT:btnLogOut:Text = "Log Out".
        THIS-OBJECT:btnLogOut:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnLogOut:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnLogOut:Click:Subscribe(THIS-OBJECT:btnLogOut_Click).
        /*  */
        /* CustomerView */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(435, 714).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnLogOut).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnCrud).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnCreateNewOrder).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtName).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblName).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnShowPreviousOrders).
        THIS-OBJECT:Name = "CustomerView".
        THIS-OBJECT:Text = "CustomerView".
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID label1_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC CustomerView ( ):

    END DESTRUCTOR.

END CLASS.