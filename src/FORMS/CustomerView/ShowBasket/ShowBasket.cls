 
/*------------------------------------------------------------------------
   File        : ShowBasket
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Tue Nov 17 09:07:35 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.BusinessEntity.beOrders FROM PROPATH.
USING src.Classes.DataAcces.Order.daOrder FROM PROPATH.
USING src.Classes.BusinessEntity.beOrderLine FROM PROPATH.
USING src.Classes.BusinessEntity.beItem FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.CustomerView.ShowBasket.ShowBasket INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsBasket.i}
    {D:\Progress2020Internship_Project\src\Shared\dsOrderline.i}
    {D:\Progress2020Internship_Project\src\Shared\dsOrder.i}
    
    DEFINE PRIVATE VARIABLE bsBasket                           AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnCreateOrder                     AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnClose                           AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE category2DataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE category1DataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE components                         AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                      AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE itemNameDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE label2                             AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblTotalPriceValue                 AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblTotalPrice                      AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblTotalWeight                     AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE priceDataGridViewTextBoxColumn     AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE itemNumDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE weightDataGridViewTextBoxColumn    AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE quantityDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE totalPriceToPay                    AS DECIMAL.
    DEFINE PRIVATE VARIABLE totalWeight                        AS DECIMAL.
    DEFINE PRIVATE VARIABLE custNum                            AS INTEGER.
    DEFINE TEMP-TABLE tttBasket NO-UNDO LIKE ttBasket.
    DEFINE DATASET ddsBasket FOR tttBasket.
    DEFINE PRIVATE VARIABLE beOrder     AS beOrders    NO-UNDO.
    DEFINE PRIVATE VARIABLE daOrder     AS daOrder     NO-UNDO.
    DEFINE PRIVATE VARIABLE beorderLine AS beOrderLine NO-UNDO.
    DEFINE PRIVATE VARIABLE beItem      AS beItem      NO-UNDO.
    
    
    
    
    
    CONSTRUCTOR PUBLIC ShowBasket (INPUT dataset dsBasket, customernumber AS INTEGER   ):
        
        InitializeComponent().
        
        custNum = customernumber.
        
        DATASET dsOrder:EMPTY-DATASET ().
        FIND FIRST ttBasket NO-ERROR.
        
        FOR EACH ttBasket :
            totalPriceToPay = totalPriceToPay + ttBasket.Price * ttBasket.Quantity.
            totalWeight = totalWeight + ttBasket.Weight * ttBasket.Quantity.
        END.
        
        
        bsBasket:handle = DATASET dsBasket:handle.
        
        lblTotalPriceValue:text = STRING(totalPriceToPay) + "$".
        lblTotalWeight:text = STRING(totalWeight).
        
        DATASET ddsBasket:COPY-DATASET (DATASET dsBasket:handle).
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnCreateOrder_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        
        DEFINE VARIABLE linenumber AS INTEGER NO-UNDO.
        DEFINE VARIABLE ordrnum    AS INTEGER NO-UNDO.
        beOrder = NEW beOrders().
        beItem = NEW beItem().
        beorderLine = NEW beOrderLine().
        DATASET dsORderline:EMPTY-DATASET ().
       
        CREATE ttOrder.
        ASSIGN
            ttOrder.Carrier     = "Urgent cargus"
            ttOrder.CreditCard  = "Visa"
            ttOrder.CustNum     = custNum
            ttOrder.OrderNum    = beOrder:getLastOrderNum() + 5
            ttOrder.OrderDate   = TODAY
            ttOrder.OrderStatus = "Ordered"
            ttOrder.PromiseDate = TODAY + 15 .
        RELEASE ttOrder.
        
        beOrder:StoreData(INPUT-OUTPUT dataset dsOrder BY-REFERENCE).
       
        FOR EACH tttBasket BY tttBasket.ItemNum:
            CREATE ttOrderLine.
            ASSIGN
                ttOrderLine.LineNum         = linenumber + 1
                ttOrderLine.ExtendedPrice   = tttBasket.Price * tttBasket.Quantity
                ttOrderLine.ItemNum         = tttBasket.ItemNum
                ttOrderLine.Price           = tttBasket.Price
                ttOrderLine.Qty             = tttBasket.Quantity
                ttOrderLine.OrderNum        = ttOrder.OrderNum
                ttOrderLine.OrderLineStatus = "Ordered".
                        
                        
            RELEASE ttOrderLine.
            linenumber = linenumber + 1.
        END.
          
          
        FOR EACH ttOrderLine BY ttOrderLine.ItemNum :
            
            beItem:UpdateAllocated(ttOrderLine.ItemNum, ttOrderLine.Qty).  
        END.
        
        beorderLine:StoreData(INPUT-OUTPUT dataset dsORderline BY-REFERENCE).
        
        
        IF ERROR-STATUS:ERROR THEN
            MESSAGE "Something went wrong"
                VIEW-AS ALERT-BOX.
        ELSE 
            MESSAGE "Order has been placed"
                VIEW-AS ALERT-BOX.
        
        DATASET dsOrder:WRITE-XML("file","D:\Progress2020Internship_Project\src\CHECK\dsORderrrrrrr.xml",TRUE).
          
        DATASET dsORderline:WRITE-XML("file","D:\Progress2020Internship_Project\src\CHECK\baaaaaaaaaaaa.xml",TRUE).
        
        
        
            
                                                                                 
    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc1 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc1 = NEW Progress.Data.TableDesc("ttBasket").
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:quantityDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:priceDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:category1DataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:category2DataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:weightDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:bsBasket = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnCreateOrder = NEW System.Windows.Forms.Button().
        THIS-OBJECT:lblTotalPrice = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblTotalPriceValue = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblTotalWeight = NEW System.Windows.Forms.Label().
        THIS-OBJECT:label2 = NEW System.Windows.Forms.Label().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:bsBasket, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS System.Windows.Forms.DataGridViewColumn EXTENT 7 NO-UNDO.
        arrayvar0[1] = THIS-OBJECT:itemNameDataGridViewTextBoxColumn.
        arrayvar0[2] = THIS-OBJECT:quantityDataGridViewTextBoxColumn.
        arrayvar0[3] = THIS-OBJECT:priceDataGridViewTextBoxColumn.
        arrayvar0[4] = THIS-OBJECT:category1DataGridViewTextBoxColumn.
        arrayvar0[5] = THIS-OBJECT:category2DataGridViewTextBoxColumn.
        arrayvar0[6] = THIS-OBJECT:itemNumDataGridViewTextBoxColumn.
        arrayvar0[7] = THIS-OBJECT:weightDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar0).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bsBasket.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(12, 12).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(769, 375).
        THIS-OBJECT:dataGridView1:TabIndex = 0.
        /*  */
        /* itemNameDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn:DataPropertyName = "ItemName".
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn:HeaderText = "Item Name".
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn:Name = "itemNameDataGridViewTextBoxColumn".
        /*  */
        /* quantityDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:quantityDataGridViewTextBoxColumn:DataPropertyName = "Quantity".
        THIS-OBJECT:quantityDataGridViewTextBoxColumn:HeaderText = "Quantity".
        THIS-OBJECT:quantityDataGridViewTextBoxColumn:Name = "quantityDataGridViewTextBoxColumn".
        /*  */
        /* priceDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:priceDataGridViewTextBoxColumn:DataPropertyName = "Price".
        THIS-OBJECT:priceDataGridViewTextBoxColumn:HeaderText = "Price".
        THIS-OBJECT:priceDataGridViewTextBoxColumn:Name = "priceDataGridViewTextBoxColumn".
        /*  */
        /* category1DataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:category1DataGridViewTextBoxColumn:DataPropertyName = "Category1".
        THIS-OBJECT:category1DataGridViewTextBoxColumn:HeaderText = "Category1".
        THIS-OBJECT:category1DataGridViewTextBoxColumn:Name = "category1DataGridViewTextBoxColumn".
        /*  */
        /* category2DataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:category2DataGridViewTextBoxColumn:DataPropertyName = "Category2".
        THIS-OBJECT:category2DataGridViewTextBoxColumn:HeaderText = "Category2".
        THIS-OBJECT:category2DataGridViewTextBoxColumn:Name = "category2DataGridViewTextBoxColumn".
        /*  */
        /* itemNumDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn:DataPropertyName = "ItemNum".
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn:HeaderText = "Item Num".
        THIS-OBJECT:itemNumDataGridViewTextBoxColumn:Name = "itemNumDataGridViewTextBoxColumn".
        /*  */
        /* weightDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:weightDataGridViewTextBoxColumn:DataPropertyName = "Weight".
        THIS-OBJECT:weightDataGridViewTextBoxColumn:HeaderText = "Weight".
        THIS-OBJECT:weightDataGridViewTextBoxColumn:Name = "weightDataGridViewTextBoxColumn".
        /*  */
        /* bsBasket */
        /*  */
        THIS-OBJECT:bsBasket:MaxDataGuess = 0.
        THIS-OBJECT:bsBasket:NoLOBs = FALSE.
        THIS-OBJECT:bsBasket:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar1 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc1:ChildTables = arrayvar1.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS Progress.Data.ColumnPropDesc EXTENT 16 NO-UNDO.
        arrayvar2[1] = NEW Progress.Data.ColumnPropDesc("Quantity", "", Progress.Data.DataType:INTEGER).
        arrayvar2[2] = NEW Progress.Data.ColumnPropDesc("Allocated", "Allocated", Progress.Data.DataType:INTEGER).
        arrayvar2[3] = NEW Progress.Data.ColumnPropDesc("CatDescription", "Cat-Description", Progress.Data.DataType:CHARACTER).
        arrayvar2[4] = NEW Progress.Data.ColumnPropDesc("Category1", "Category1", Progress.Data.DataType:CHARACTER).
        arrayvar2[5] = NEW Progress.Data.ColumnPropDesc("Category2", "Category2", Progress.Data.DataType:CHARACTER).
        arrayvar2[6] = NEW Progress.Data.ColumnPropDesc("CatPage", "Cat Page", Progress.Data.DataType:INTEGER).
        arrayvar2[7] = NEW Progress.Data.ColumnPropDesc("ItemImage", "null", Progress.Data.DataType:BLOB).
        arrayvar2[8] = NEW Progress.Data.ColumnPropDesc("ItemName", "Item Name", Progress.Data.DataType:CHARACTER).
        arrayvar2[9] = NEW Progress.Data.ColumnPropDesc("ItemNum", "Item Num", Progress.Data.DataType:INTEGER).
        arrayvar2[10] = NEW Progress.Data.ColumnPropDesc("MinQty", "Min Qty", Progress.Data.DataType:INTEGER).
        arrayvar2[11] = NEW Progress.Data.ColumnPropDesc("OnHand", "On Hand", Progress.Data.DataType:INTEGER).
        arrayvar2[12] = NEW Progress.Data.ColumnPropDesc("OnOrder", "On Order", Progress.Data.DataType:INTEGER).
        arrayvar2[13] = NEW Progress.Data.ColumnPropDesc("Price", "Price", Progress.Data.DataType:DECIMAL).
        arrayvar2[14] = NEW Progress.Data.ColumnPropDesc("ReOrder", "Re Order", Progress.Data.DataType:INTEGER).
        arrayvar2[15] = NEW Progress.Data.ColumnPropDesc("Special", "Special", Progress.Data.DataType:CHARACTER).
        arrayvar2[16] = NEW Progress.Data.ColumnPropDesc("Weight", "Weight", Progress.Data.DataType:DECIMAL).
        tableDesc1:Columns = arrayvar2.
        THIS-OBJECT:bsBasket:TableSchema = tableDesc1.
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(12, 496).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(138, 59).
        THIS-OBJECT:btnClose:TabIndex = 1.
        THIS-OBJECT:btnClose:Text = "Close".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* btnCreateOrder */
        /*  */
        THIS-OBJECT:btnCreateOrder:Location = NEW System.Drawing.Point(643, 496).
        THIS-OBJECT:btnCreateOrder:Name = "btnCreateOrder".
        THIS-OBJECT:btnCreateOrder:Size = NEW System.Drawing.Size(138, 59).
        THIS-OBJECT:btnCreateOrder:TabIndex = 2.
        THIS-OBJECT:btnCreateOrder:Text = "Create Order".
        THIS-OBJECT:btnCreateOrder:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnCreateOrder:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnCreateOrder:Click:Subscribe(THIS-OBJECT:btnCreateOrder_Click).
        /*  */
        /* lblTotalPrice */
        /*  */
        THIS-OBJECT:lblTotalPrice:Location = NEW System.Drawing.Point(33, 411).
        THIS-OBJECT:lblTotalPrice:Name = "lblTotalPrice".
        THIS-OBJECT:lblTotalPrice:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblTotalPrice:TabIndex = 3.
        THIS-OBJECT:lblTotalPrice:Text = "Total Price:".
        THIS-OBJECT:lblTotalPrice:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblTotalPriceValue */
        /*  */
        THIS-OBJECT:lblTotalPriceValue:Location = NEW System.Drawing.Point(106, 411).
        THIS-OBJECT:lblTotalPriceValue:Name = "lblTotalPriceValue".
        THIS-OBJECT:lblTotalPriceValue:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblTotalPriceValue:TabIndex = 4.
        THIS-OBJECT:lblTotalPriceValue:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblTotalWeight */
        /*  */
        THIS-OBJECT:lblTotalWeight:Location = NEW System.Drawing.Point(106, 434).
        THIS-OBJECT:lblTotalWeight:Name = "lblTotalWeight".
        THIS-OBJECT:lblTotalWeight:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblTotalWeight:TabIndex = 6.
        THIS-OBJECT:lblTotalWeight:UseCompatibleTextRendering = TRUE.
        /*  */
        /* label2 */
        /*  */
        THIS-OBJECT:label2:Location = NEW System.Drawing.Point(33, 434).
        THIS-OBJECT:label2:Name = "label2".
        THIS-OBJECT:label2:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:label2:TabIndex = 5.
        THIS-OBJECT:label2:Text = "Total Weight:".
        THIS-OBJECT:label2:UseCompatibleTextRendering = TRUE.
        /*  */
        /* ShowBasket */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(799, 567).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblTotalWeight).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:label2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblTotalPriceValue).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblTotalPrice).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnCreateOrder).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "ShowBasket".
        THIS-OBJECT:Text = "ShowBasket".
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:bsBasket, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC ShowBasket ( ):

    END DESTRUCTOR.

END CLASS.