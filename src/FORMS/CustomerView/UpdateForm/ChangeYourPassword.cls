 
/*------------------------------------------------------------------------
   File        : ChangeYourPassword
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Mon Nov 16 06:55:13 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.BusinessEntity.beCustomer FROM PROPATH.
USING OpenEdge.Core.Assert FROM PROPATH.
USING src.Classes.Utils.StringValidator FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.CustomerView.UpdateForm.ChangeYourPassword INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnGoBack          AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnSave            AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components         AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE lblNewPassword     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblNewPAsswrodConf AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblOldPassword     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE txtNewPasswordConf AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE txtNewPassword     AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textOldPassword    AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE beCustomer         AS beCustomer                       NO-UNDO.
    DEFINE PRIVATE VARIABLE customerNumber     AS INTEGER                          NO-UNDO.

    CONSTRUCTOR PUBLIC ChangeYourPassword ( INPUT custnum AS INTEGER  ):
        
        InitializeComponent().
        beCustomer = NEW beCustomer().
        customerNumber = custnum.
        beCustomer:getCustomer(OUTPUT dataset dsCustomer BY-REFERENCE, customerNumber).
        
        FIND FIRST ttCustomer EXCLUSIVE-LOCK NO-ERROR.
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnGoBack_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnSave_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        
        
        IF ttCustomer.Password = encode(textOldPassword:text) THEN 
        DO:
            
            Assert:NotEmpty(txtNewPassword:text).
            
            IF  StringValidator:passwordNeedsSpecialChar(txtNewPassword:text) THEN
            DO:
                MESSAGE "New password must contain at least one special character"
                    VIEW-AS ALERT-BOX.
                UNDO,RETRY.
            END.
        
            IF LENGTH (txtNewPassword:text) < 8 THEN 
            DO:
                MESSAGE "Length of the password must be at least 8 characters long."
                    VIEW-AS ALERT-BOX.
                UNDO,RETRY.
            END.
        
            IF txtNewPassword:text NE txtNewPasswordConf:text THEN 
            DO:
                MESSAGE "Passwords must match"
                    VIEW-AS ALERT-BOX.
                UNDO, RETRY.
            END.
            
            ASSIGN 
                ttCustomer.Password = txtNewPassword:text.
                
            beCustomer:UpdateCustomerPassword(INPUT-OUTPUT DATASET dsCustomer BY-REFERENCE).
            
            MESSAGE "You have changed your password successfully!"
            VIEW-AS ALERT-BOX.
            
        END.
        
        ELSE 
        DO:
            MESSAGE "Old password must match"
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:btnSave = NEW System.Windows.Forms.Button().
        THIS-OBJECT:lblOldPassword = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblNewPassword = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblNewPAsswrodConf = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textOldPassword = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:txtNewPassword = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:txtNewPasswordConf = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:btnGoBack = NEW System.Windows.Forms.Button().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* btnSave */
        /*  */
        THIS-OBJECT:btnSave:Location = NEW System.Drawing.Point(108, 155).
        THIS-OBJECT:btnSave:Name = "btnSave".
        THIS-OBJECT:btnSave:Size = NEW System.Drawing.Size(250, 62).
        THIS-OBJECT:btnSave:TabIndex = 0.
        THIS-OBJECT:btnSave:Text = "Save your new password".
        THIS-OBJECT:btnSave:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnSave:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnSave:Click:Subscribe(THIS-OBJECT:btnSave_Click).
        /*  */
        /* lblOldPassword */
        /*  */
        THIS-OBJECT:lblOldPassword:Location = NEW System.Drawing.Point(12, 42).
        THIS-OBJECT:lblOldPassword:Name = "lblOldPassword".
        THIS-OBJECT:lblOldPassword:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblOldPassword:TabIndex = 1.
        THIS-OBJECT:lblOldPassword:Text = "Old Pasword:".
        THIS-OBJECT:lblOldPassword:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblNewPassword */
        /*  */
        THIS-OBJECT:lblNewPassword:Location = NEW System.Drawing.Point(12, 68).
        THIS-OBJECT:lblNewPassword:Name = "lblNewPassword".
        THIS-OBJECT:lblNewPassword:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblNewPassword:TabIndex = 2.
        THIS-OBJECT:lblNewPassword:Text = "New Password:".
        THIS-OBJECT:lblNewPassword:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblNewPassword:Click:Subscribe(THIS-OBJECT:label2_Click).
        /*  */
        /* lblNewPAsswrodConf */
        /*  */
        THIS-OBJECT:lblNewPAsswrodConf:Location = NEW System.Drawing.Point(12, 94).
        THIS-OBJECT:lblNewPAsswrodConf:Name = "lblNewPAsswrodConf".
        THIS-OBJECT:lblNewPAsswrodConf:Size = NEW System.Drawing.Size(158, 23).
        THIS-OBJECT:lblNewPAsswrodConf:TabIndex = 3.
        THIS-OBJECT:lblNewPAsswrodConf:Text = "New Password Confirmation:".
        THIS-OBJECT:lblNewPAsswrodConf:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textOldPassword */
        /*  */
        THIS-OBJECT:textOldPassword:Location = NEW System.Drawing.Point(216, 39).
        THIS-OBJECT:textOldPassword:Name = "textOldPassword".
        THIS-OBJECT:textOldPassword:PasswordChar = '*'.
        THIS-OBJECT:textOldPassword:Size = NEW System.Drawing.Size(172, 20).
        THIS-OBJECT:textOldPassword:TabIndex = 4.
        /*  */
        /* txtNewPassword */
        /*  */
        THIS-OBJECT:txtNewPassword:Location = NEW System.Drawing.Point(216, 65).
        THIS-OBJECT:txtNewPassword:Name = "txtNewPassword".
        THIS-OBJECT:txtNewPassword:PasswordChar = '*'.
        THIS-OBJECT:txtNewPassword:Size = NEW System.Drawing.Size(172, 20).
        THIS-OBJECT:txtNewPassword:TabIndex = 5.
        /*  */
        /* txtNewPasswordConf */
        /*  */
        THIS-OBJECT:txtNewPasswordConf:Location = NEW System.Drawing.Point(216, 91).
        THIS-OBJECT:txtNewPasswordConf:Name = "txtNewPasswordConf".
        THIS-OBJECT:txtNewPasswordConf:PasswordChar = '*'.
        THIS-OBJECT:txtNewPasswordConf:Size = NEW System.Drawing.Size(172, 20).
        THIS-OBJECT:txtNewPasswordConf:TabIndex = 6.
        /*  */
        /* btnGoBack */
        /*  */
        THIS-OBJECT:btnGoBack:Location = NEW System.Drawing.Point(108, 223).
        THIS-OBJECT:btnGoBack:Name = "btnGoBack".
        THIS-OBJECT:btnGoBack:Size = NEW System.Drawing.Size(250, 62).
        THIS-OBJECT:btnGoBack:TabIndex = 7.
        THIS-OBJECT:btnGoBack:Text = "Go Back".
        THIS-OBJECT:btnGoBack:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnGoBack:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnGoBack:Click:Subscribe(THIS-OBJECT:btnGoBack_Click).
        /*  */
        /* ChangeYourPassword */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(467, 293).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnGoBack).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtNewPasswordConf).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtNewPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textOldPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblNewPAsswrodConf).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblNewPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblOldPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnSave).
        THIS-OBJECT:Name = "ChangeYourPassword".
        THIS-OBJECT:Text = "ChangeYourPassword".
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID label2_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC ChangeYourPassword ( ):

    END DESTRUCTOR.

END CLASS.