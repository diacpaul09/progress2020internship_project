 
/*------------------------------------------------------------------------
   File        : CreateANewOrder
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Mon Nov 16 14:14:01 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.BusinessEntity.beItem FROM PROPATH.
USING src.Classes.DataAcces.Item.daItem FROM PROPATH.

USING src.FORMS.CustomerView.ShowBasket.ShowBasket FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.CustomerView.CreateANewOrder.CreateANewOrder INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsItem.i            }
    {D:\Progress2020Internship_Project\src\Shared\dsCategory.i        }
    {D:\Progress2020Internship_Project\src\Shared\dsCategory2.i       }
    {D:\Progress2020Internship_Project\src\Shared\dsSelectedItem.i}
    
    {D:\Progress2020Internship_Project\src\Shared\dsBasket.i}
    DEFINE PRIVATE VARIABLE bsCategory                        AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE bsCategory2                       AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE bsItemsToShow                     AS Progress.Data.BindingSource                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnAddToBasket                    AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnClose                          AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnLoadImage                      AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnSavePhoto                      AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE btnShowBasket                     AS System.Windows.Forms.Button                    NO-UNDO.
    DEFINE PRIVATE VARIABLE comboBoxCategory2                 AS System.Windows.Forms.ComboBox                  NO-UNDO.
    DEFINE PRIVATE VARIABLE comboBoxCategory1                 AS System.Windows.Forms.ComboBox                  NO-UNDO.
    DEFINE PRIVATE VARIABLE components                        AS System.ComponentModel.IContainer               NO-UNDO.
    DEFINE PRIVATE VARIABLE beItem                            AS beItem                                         NO-UNDO.
    DEFINE PRIVATE VARIABLE daItem                            AS daItem                                         NO-UNDO.
    DEFINE PRIVATE VARIABLE dataGridView1                     AS System.Windows.Forms.DataGridView              NO-UNDO.
    DEFINE PRIVATE VARIABLE custNum                           AS INTEGER.
    DEFINE PRIVATE VARIABLE lblQty                            AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblQTYvalue                       AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPriceValue                     AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPrice                          AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblName                           AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblItemName                       AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblDescrp                         AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblDescription                    AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblAllocated                      AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE label1                            AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblSportType                      AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblEqType                         AS System.Windows.Forms.Label                     NO-UNDO.
    DEFINE PRIVATE VARIABLE numericUpDown1                    AS System.Windows.Forms.NumericUpDown             NO-UNDO.
    DEFINE PRIVATE VARIABLE pictureBox1                       AS System.Windows.Forms.PictureBox                NO-UNDO.
    DEFINE PRIVATE VARIABLE weightDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE specialDataGridViewTextBoxColumn  AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE priceDataGridViewTextBoxColumn    AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE minQtyDataGridViewTextBoxColumn   AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE itemNameDataGridViewTextBoxColumn AS System.Windows.Forms.DataGridViewTextBoxColumn NO-UNDO.
    DEFINE PRIVATE VARIABLE showBasket                        AS ShowBasket                                     NO-UNDO.
    
    
    DEFINE         VARIABLE selectedImageFromPath             AS CHARACTER                                      NO-UNDO.
    
    CONSTRUCTOR PUBLIC CreateANewOrder ( INPUT customernumber AS INTEGER ):
        
        InitializeComponent().
        
        custNum = customernumber.
        
        DATASET dsBasket:EMPTY-DATASET ().
        dataGridView1:Hide().
        
        beItem = NEW beItem().
        beItem:getItem(OUTPUT dataset dsItem BY-REFERENCE).
        
        FOR EACH ttItem BREAK BY ttItem.Category2:
            IF FIRST-OF(ttItem.Category2) THEN 
            DO:
                CREATE ttCategory2.
                ASSIGN 
                    ttCategory2.Category2 = ttItem.Category2.
                RELEASE ttCategory.
            END.
            
        END.
        
        bsCategory2:handle = DATASET dsCategory2:HANDLE.
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.


    

    METHOD PUBLIC VOID AddToBasket( INPUT itemNumber AS INTEGER , INPUT quantity AS INTEGER):
        
        
        
        
        FIND FIRST ttSelectedItem WHERE ttSelectedItem.ItemNum = itemNumber NO-ERROR.


        IF AVAILABLE ttSelectedItem THEN
        DO:
            
            FIND FIRST  ttBasket WHERE ttBasket.ItemNum = itemNumber NO-ERROR.
            IF AVAILABLE ttBasket THEN 
            DO:
                ttBasket.Quantity = quantity.
                MESSAGE "You already have this item in your basket. What you can do, is to change the quantity of this item you want to buy. Currently it is set to : " + string(ttBasket.Quantity)
                    VIEW-AS ALERT-BOX.
            END.
            ELSE 
            DO:
                CREATE ttBasket.
                BUFFER-COPY ttSelectedItem TO ttBasket.
                ttBasket.Quantity = quantity.
                RELEASE ttBasket.
                MESSAGE "Items added to your basket"
                    VIEW-AS ALERT-BOX.
            END.
        END.



        DATASET dsBasket:WRITE-XML ("file","D:\Progress2020Internship_Project\src\CHECK\IHOpeIhaveSomethingIn.xml",TRUE).

    END METHOD.

    
    @VisualDesigner.
    METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		      
        DELETE OBJECT THIS-OBJECT.

    END METHOD.


    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnLoadImage_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DEFINE VARIABLE isOK         AS LOGICAL   NO-UNDO.
      
        DEFINE VARIABLE errorMessage AS CHARACTER NO-UNDO.
  
        SYSTEM-DIALOG GET-FILE selectedImageFromPath
            TITLE "Select the image.."
            UPDATE isOK.
  
        IF (NOT isOK) THEN 
            RETURN. 
 
        IF (selectedImageFromPath <> "") AND
            (selectedImageFromPath <> ?) THEN 
        DO:
    

            pictureBox1:Load(selectedImageFromPath)  NO-ERROR.  
           
        END.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnSavePhoto_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        /*        FIND FIRST ttSelectedItem.                                           */
        /*                                                                             */
        /*        MESSAGE Item.ItemNum                                                 */
        /*            VIEW-AS ALERT-BOX.                                               */
        IF AVAILABLE ttSelectedItem THEN 
        DO:
            FIND FIRST Item WHERE Item.ItemNum = ttSelectedItem.ItemNum NO-ERROR.

            IF pictureBox1:Image <> ? AND selectedImageFromPath <> "" THEN 
            DO:
                COPY-LOB FROM FILE selectedImageFromPath TO item.ItemImage .
                
            END. 
        END.
            
        
		
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnShowBasket_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        DATASET dsBasket:TOP-NAV-QUERY:query-open (). 
        showBasket = NEW ShowBasket(INPUT dataset dsBasket BY-REFERENCE, custNum).
        showBasket:show().
		

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID button1_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		 
        IF INTEGER (numericUpDown1:text) GE ttSelectedItem.MinQty THEN 
        DO:
            IF ttSelectedItem.Allocated - INTEGER (numericUpDown1:text) GE 0 THEN 
            DO:
                
                AddToBasket(ttSelectedItem.ItemNum, INTEGER (numericUpDown1:text)).
              
            END.
            ELSE 
            DO:
                MESSAGE "Stock supply can not handle this amount of items"
                    VIEW-AS ALERT-BOX.
                UNDO,RETRY.
            END.
        END.
        ELSE 
        DO:
            MESSAGE "Selected quantity does not respect the minimum quantity condition"
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID comboBox1_SelectedIndexChanged( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        
        
        DATASET dsCategory:EMPTY-DATASET ().
        
        
        
        FOR EACH ttItem WHERE ttItem.Category2 = comboBoxCategory2:text BREAK BY ttItem.Category1:
            IF FIRST-OF(ttItem.Category1) THEN 
            DO:
                CREATE ttCategory.
                ASSIGN 
                    ttCategory.Category = ttItem.Category1.
                RELEASE ttCategory.
            END.
            
        END.
        DATASET dsCategory:TOP-NAV-QUERY:query-open (). 
        bsCategory:handle = DATASET dsCategory:HANDLE.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID comboBoxCategory1_SelectedIndexChanged( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        
        DATASET dsSelectedItem:EMPTY-DATASET ().
        dataGridView1:Show().
       

        FOR EACH ttItem WHERE ttItem.Category1 = comboBoxCategory1:text AND ttItem.Category2 = comboBoxCategory2:text:
            CREATE ttSelectedItem.
            BUFFER-COPY ttItem TO ttSelectedItem.
            RELEASE ttSelectedItem.
        END. 
		
        
        DATASET dsSelectedItem:TOP-NAV-QUERY:query-open (). 
        bsItemsToShow:handle = DATASET dsSelectedItem:HANDLE.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_CellContentClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.DataGridViewCellEventArgs ):
		

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID dataGridView1_MouseClick( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.MouseEventArgs ):
        pictureBox1:Image= ? .
        pictureBox1:Refresh().
	
        IF AVAILABLE ttSelectedItem THEN 
        DO:
            DEFINE VARIABLE filepath AS CHARACTER NO-UNDO.
            ASSIGN 
                
                lblName:text       = ttSelectedItem.ItemName
                lblDescrp:text     = ttSelectedItem.CatDescription
                lblPriceValue:text = STRING(ttSelectedItem.Price) + "$"
                lblQTYvalue:text   = "*Minimun selected quantity for this item must be " + string(ttSelectedItem.MinQty) 
                lblAllocated:text  = STRING(ttSelectedItem.Allocated)

                .
                
                
           
            
            IF ttSelectedItem.ItemImage <> ? THEN 
            DO:
                filepath =SESSION:TEMP-DIRECTORY + "tempImg" + STRING(MTIME) + ".png".
                COPY-LOB FROM ttSelectedItem.ItemImage TO FILE filepath.
                pictureBox1:Load(filepath).
             
            END.
            ELSE 
            DO:
                pictureBox1:image = ?.
            END.
            
            
        END. 
        
    END METHOD.
    
    

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
            
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
            
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:components = NEW System.ComponentModel.Container().
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc4 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc4 = NEW Progress.Data.TableDesc("ttSelectedItem").
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc5 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc5 = NEW Progress.Data.TableDesc("ttCategory2").
        @VisualDesigner.FormMember (NeedsInitialize="true").
        DEFINE VARIABLE tableDesc6 AS Progress.Data.TableDesc NO-UNDO.
        tableDesc6 = NEW Progress.Data.TableDesc("ttCategory").
        THIS-OBJECT:dataGridView1 = NEW System.Windows.Forms.DataGridView().
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:priceDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:specialDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:minQtyDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:weightDataGridViewTextBoxColumn = NEW System.Windows.Forms.DataGridViewTextBoxColumn().
        THIS-OBJECT:bsItemsToShow = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        THIS-OBJECT:bsCategory2 = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:comboBoxCategory2 = NEW System.Windows.Forms.ComboBox().
        THIS-OBJECT:comboBoxCategory1 = NEW System.Windows.Forms.ComboBox().
        THIS-OBJECT:bsCategory = NEW Progress.Data.BindingSource(THIS-OBJECT:components).
        THIS-OBJECT:lblEqType = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblSportType = NEW System.Windows.Forms.Label().
        THIS-OBJECT:btnShowBasket = NEW System.Windows.Forms.Button().
        THIS-OBJECT:lblAllocated = NEW System.Windows.Forms.Label().
        THIS-OBJECT:label1 = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblQTYvalue = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblQty = NEW System.Windows.Forms.Label().
        THIS-OBJECT:numericUpDown1 = NEW System.Windows.Forms.NumericUpDown().
        THIS-OBJECT:lblPriceValue = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblPrice = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblDescrp = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblName = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblDescription = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblItemName = NEW System.Windows.Forms.Label().
        THIS-OBJECT:btnAddToBasket = NEW System.Windows.Forms.Button().
        THIS-OBJECT:pictureBox1 = NEW System.Windows.Forms.PictureBox().
        THIS-OBJECT:btnLoadImage = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnSavePhoto = NEW System.Windows.Forms.Button().
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:bsItemsToShow, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:bsCategory2, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:bsCategory, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:numericUpDown1, System.ComponentModel.ISupportInitialize):BeginInit().
        CAST(THIS-OBJECT:pictureBox1, System.ComponentModel.ISupportInitialize):BeginInit().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* dataGridView1 */
        /*  */
        THIS-OBJECT:dataGridView1:AllowUserToOrderColumns = TRUE.
        THIS-OBJECT:dataGridView1:AutoGenerateColumns = FALSE.
        THIS-OBJECT:dataGridView1:ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode:AutoSize.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar0 AS System.Windows.Forms.DataGridViewColumn EXTENT 5 NO-UNDO.
        arrayvar0[1] = THIS-OBJECT:itemNameDataGridViewTextBoxColumn.
        arrayvar0[2] = THIS-OBJECT:priceDataGridViewTextBoxColumn.
        arrayvar0[3] = THIS-OBJECT:specialDataGridViewTextBoxColumn.
        arrayvar0[4] = THIS-OBJECT:minQtyDataGridViewTextBoxColumn.
        arrayvar0[5] = THIS-OBJECT:weightDataGridViewTextBoxColumn.
        THIS-OBJECT:dataGridView1:Columns:AddRange(arrayvar0).
        THIS-OBJECT:dataGridView1:DataSource = THIS-OBJECT:bsItemsToShow.
        THIS-OBJECT:dataGridView1:Location = NEW System.Drawing.Point(12, 104).
        THIS-OBJECT:dataGridView1:Name = "dataGridView1".
        THIS-OBJECT:dataGridView1:Size = NEW System.Drawing.Size(544, 247).
        THIS-OBJECT:dataGridView1:TabIndex = 1.
        THIS-OBJECT:dataGridView1:CellContentClick:Subscribe(THIS-OBJECT:dataGridView1_CellContentClick).
        THIS-OBJECT:dataGridView1:MouseClick:Subscribe(THIS-OBJECT:dataGridView1_MouseClick).
        /*  */
        /* itemNameDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn:DataPropertyName = "ItemName".
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn:HeaderText = "Item Name".
        THIS-OBJECT:itemNameDataGridViewTextBoxColumn:Name = "itemNameDataGridViewTextBoxColumn".
        /*  */
        /* priceDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:priceDataGridViewTextBoxColumn:DataPropertyName = "Price".
        THIS-OBJECT:priceDataGridViewTextBoxColumn:HeaderText = "Price".
        THIS-OBJECT:priceDataGridViewTextBoxColumn:Name = "priceDataGridViewTextBoxColumn".
        /*  */
        /* specialDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:specialDataGridViewTextBoxColumn:DataPropertyName = "Special".
        THIS-OBJECT:specialDataGridViewTextBoxColumn:HeaderText = "Special".
        THIS-OBJECT:specialDataGridViewTextBoxColumn:Name = "specialDataGridViewTextBoxColumn".
        /*  */
        /* minQtyDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:minQtyDataGridViewTextBoxColumn:DataPropertyName = "MinQty".
        THIS-OBJECT:minQtyDataGridViewTextBoxColumn:HeaderText = "Min Qty".
        THIS-OBJECT:minQtyDataGridViewTextBoxColumn:Name = "minQtyDataGridViewTextBoxColumn".
        /*  */
        /* weightDataGridViewTextBoxColumn */
        /*  */
        THIS-OBJECT:weightDataGridViewTextBoxColumn:DataPropertyName = "Weight".
        THIS-OBJECT:weightDataGridViewTextBoxColumn:HeaderText = "Weight".
        THIS-OBJECT:weightDataGridViewTextBoxColumn:Name = "weightDataGridViewTextBoxColumn".
        /*  */
        /* bsItemsToShow */
        /*  */
        THIS-OBJECT:bsItemsToShow:MaxDataGuess = 0.
        THIS-OBJECT:bsItemsToShow:NoLOBs = FALSE.
        THIS-OBJECT:bsItemsToShow:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar1 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar1 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc4:ChildTables = arrayvar1.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar2 AS Progress.Data.ColumnPropDesc EXTENT 8 NO-UNDO.
        arrayvar2[1] = NEW Progress.Data.ColumnPropDesc("CatDescription", "Cat-Description", Progress.Data.DataType:CHARACTER).
        arrayvar2[2] = NEW Progress.Data.ColumnPropDesc("ItemImage", "null", Progress.Data.DataType:BLOB).
        arrayvar2[3] = NEW Progress.Data.ColumnPropDesc("ItemName", "Item Name", Progress.Data.DataType:CHARACTER).
        arrayvar2[4] = NEW Progress.Data.ColumnPropDesc("ItemNum", "Item Num", Progress.Data.DataType:INTEGER).
        arrayvar2[5] = NEW Progress.Data.ColumnPropDesc("MinQty", "Min Qty", Progress.Data.DataType:INTEGER).
        arrayvar2[6] = NEW Progress.Data.ColumnPropDesc("Price", "Price", Progress.Data.DataType:DECIMAL).
        arrayvar2[7] = NEW Progress.Data.ColumnPropDesc("Special", "Special", Progress.Data.DataType:CHARACTER).
        arrayvar2[8] = NEW Progress.Data.ColumnPropDesc("Weight", "Weight", Progress.Data.DataType:DECIMAL).
        tableDesc4:Columns = arrayvar2.
        THIS-OBJECT:bsItemsToShow:TableSchema = tableDesc4.
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(11, 742).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(233, 49).
        THIS-OBJECT:btnClose:TabIndex = 2.
        THIS-OBJECT:btnClose:Text = "Close".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* bsCategory2 */
        /*  */
        THIS-OBJECT:bsCategory2:MaxDataGuess = 0.
        THIS-OBJECT:bsCategory2:NoLOBs = FALSE.
        THIS-OBJECT:bsCategory2:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar3 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar3 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc5:ChildTables = arrayvar3.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar4 AS Progress.Data.ColumnPropDesc EXTENT 1 NO-UNDO.
        arrayvar4[1] = NEW Progress.Data.ColumnPropDesc("Category2", "Category2", Progress.Data.DataType:CHARACTER).
        tableDesc5:Columns = arrayvar4.
        THIS-OBJECT:bsCategory2:TableSchema = tableDesc5.
        /*  */
        /* comboBoxCategory2 */
        /*  */
        THIS-OBJECT:comboBoxCategory2:DataSource = THIS-OBJECT:bsCategory2.
        THIS-OBJECT:comboBoxCategory2:DisplayMember = "Category2".
        THIS-OBJECT:comboBoxCategory2:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:comboBoxCategory2:FormattingEnabled = TRUE.
        THIS-OBJECT:comboBoxCategory2:Location = NEW System.Drawing.Point(375, 26).
        THIS-OBJECT:comboBoxCategory2:Name = "comboBoxCategory2".
        THIS-OBJECT:comboBoxCategory2:Size = NEW System.Drawing.Size(121, 28).
        THIS-OBJECT:comboBoxCategory2:TabIndex = 3.
        THIS-OBJECT:comboBoxCategory2:ValueMember = "Category2".
        THIS-OBJECT:comboBoxCategory2:SelectedIndexChanged:Subscribe(THIS-OBJECT:comboBox1_SelectedIndexChanged).
        /*  */
        /* comboBoxCategory1 */
        /*  */
        THIS-OBJECT:comboBoxCategory1:DataSource = THIS-OBJECT:bsCategory.
        THIS-OBJECT:comboBoxCategory1:DisplayMember = "Category".
        THIS-OBJECT:comboBoxCategory1:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:comboBoxCategory1:FormattingEnabled = TRUE.
        THIS-OBJECT:comboBoxCategory1:Location = NEW System.Drawing.Point(375, 70).
        THIS-OBJECT:comboBoxCategory1:Name = "comboBoxCategory1".
        THIS-OBJECT:comboBoxCategory1:Size = NEW System.Drawing.Size(121, 28).
        THIS-OBJECT:comboBoxCategory1:TabIndex = 4.
        THIS-OBJECT:comboBoxCategory1:ValueMember = "Category".
        THIS-OBJECT:comboBoxCategory1:SelectedIndexChanged:Subscribe(THIS-OBJECT:comboBoxCategory1_SelectedIndexChanged).
        /*  */
        /* bsCategory */
        /*  */
        THIS-OBJECT:bsCategory:MaxDataGuess = 0.
        THIS-OBJECT:bsCategory:NoLOBs = FALSE.
        THIS-OBJECT:bsCategory:Position = 0.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar5 AS "Progress.Data.TableDesc[]" NO-UNDO.
        arrayvar5 = NEW "Progress.Data.TableDesc[]"(0).
        tableDesc6:ChildTables = arrayvar5.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true").
        DEFINE VARIABLE arrayvar6 AS Progress.Data.ColumnPropDesc EXTENT 1 NO-UNDO.
        arrayvar6[1] = NEW Progress.Data.ColumnPropDesc("Category", "Category1", Progress.Data.DataType:CHARACTER).
        tableDesc6:Columns = arrayvar6.
        THIS-OBJECT:bsCategory:TableSchema = tableDesc6.
        /*  */
        /* lblEqType */
        /*  */
        THIS-OBJECT:lblEqType:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:lblEqType:Location = NEW System.Drawing.Point(31, 29).
        THIS-OBJECT:lblEqType:Name = "lblEqType".
        THIS-OBJECT:lblEqType:Size = NEW System.Drawing.Size(323, 23).
        THIS-OBJECT:lblEqType:TabIndex = 5.
        THIS-OBJECT:lblEqType:Text = "Select your equipment type:".
        THIS-OBJECT:lblEqType:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblSportType */
        /*  */
        THIS-OBJECT:lblSportType:Font = NEW System.Drawing.Font("Microsoft Sans Serif", Progress.Util.CastUtil:ToSingle(12), System.Drawing.FontStyle:Regular, System.Drawing.GraphicsUnit:Point, System.Convert:ToByte(0)).
        THIS-OBJECT:lblSportType:Location = NEW System.Drawing.Point(31, 73).
        THIS-OBJECT:lblSportType:Name = "lblSportType".
        THIS-OBJECT:lblSportType:Size = NEW System.Drawing.Size(323, 23).
        THIS-OBJECT:lblSportType:TabIndex = 6.
        THIS-OBJECT:lblSportType:Text = "Select the type of sport:".
        THIS-OBJECT:lblSportType:UseCompatibleTextRendering = TRUE.
        /*  */
        /* btnShowBasket */
        /*  */
        THIS-OBJECT:btnShowBasket:Location = NEW System.Drawing.Point(323, 742).
        THIS-OBJECT:btnShowBasket:Name = "btnShowBasket".
        THIS-OBJECT:btnShowBasket:Size = NEW System.Drawing.Size(233, 49).
        THIS-OBJECT:btnShowBasket:TabIndex = 7.
        THIS-OBJECT:btnShowBasket:Text = "Show my basket".
        THIS-OBJECT:btnShowBasket:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnShowBasket:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnShowBasket:Click:Subscribe(THIS-OBJECT:btnShowBasket_Click).
        /*  */
        /* lblAllocated */
        /*  */
        THIS-OBJECT:lblAllocated:Location = NEW System.Drawing.Point(124, 538).
        THIS-OBJECT:lblAllocated:Name = "lblAllocated".
        THIS-OBJECT:lblAllocated:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblAllocated:TabIndex = 24.
        THIS-OBJECT:lblAllocated:UseCompatibleTextRendering = TRUE.
        /*  */
        /* label1 */
        /*  */
        THIS-OBJECT:label1:Location = NEW System.Drawing.Point(18, 538).
        THIS-OBJECT:label1:Name = "label1".
        THIS-OBJECT:label1:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:label1:TabIndex = 23.
        THIS-OBJECT:label1:Text = "Available Items: ".
        THIS-OBJECT:label1:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblQTYvalue */
        /*  */
        THIS-OBJECT:lblQTYvalue:Location = NEW System.Drawing.Point(18, 609).
        THIS-OBJECT:lblQTYvalue:Name = "lblQTYvalue".
        THIS-OBJECT:lblQTYvalue:Size = NEW System.Drawing.Size(226, 38).
        THIS-OBJECT:lblQTYvalue:TabIndex = 22.
        THIS-OBJECT:lblQTYvalue:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblQty */
        /*  */
        THIS-OBJECT:lblQty:Location = NEW System.Drawing.Point(18, 582).
        THIS-OBJECT:lblQty:Name = "lblQty".
        THIS-OBJECT:lblQty:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblQty:TabIndex = 21.
        THIS-OBJECT:lblQty:Text = "Quantity:".
        THIS-OBJECT:lblQty:UseCompatibleTextRendering = TRUE.
        /*  */
        /* numericUpDown1 */
        /*  */
        THIS-OBJECT:numericUpDown1:Location = NEW System.Drawing.Point(124, 582).
        THIS-OBJECT:numericUpDown1:Name = "numericUpDown1".
        THIS-OBJECT:numericUpDown1:Size = NEW System.Drawing.Size(120, 20).
        THIS-OBJECT:numericUpDown1:TabIndex = 20.
        @VisualDesigner.FormMember (NeedsInitialize="false", InitializeArray="true", Type="System.Int32").
        DEFINE VARIABLE arrayvar7 AS INTEGER EXTENT 4 NO-UNDO.
        arrayvar7[1] = 1.
        arrayvar7[2] = 0.
        arrayvar7[3] = 0.
        arrayvar7[4] = 0.
        THIS-OBJECT:numericUpDown1:Value = Progress.Util.DecimalHelper:Create(arrayvar7).
        /*  */
        /* lblPriceValue */
        /*  */
        THIS-OBJECT:lblPriceValue:Location = NEW System.Drawing.Point(124, 496).
        THIS-OBJECT:lblPriceValue:Name = "lblPriceValue".
        THIS-OBJECT:lblPriceValue:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblPriceValue:TabIndex = 19.
        THIS-OBJECT:lblPriceValue:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblPrice */
        /*  */
        THIS-OBJECT:lblPrice:Location = NEW System.Drawing.Point(18, 496).
        THIS-OBJECT:lblPrice:Name = "lblPrice".
        THIS-OBJECT:lblPrice:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblPrice:TabIndex = 18.
        THIS-OBJECT:lblPrice:Text = "Price / Item:".
        THIS-OBJECT:lblPrice:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblDescrp */
        /*  */
        THIS-OBJECT:lblDescrp:Location = NEW System.Drawing.Point(104, 410).
        THIS-OBJECT:lblDescrp:Name = "lblDescrp".
        THIS-OBJECT:lblDescrp:Size = NEW System.Drawing.Size(178, 86).
        THIS-OBJECT:lblDescrp:TabIndex = 17.
        THIS-OBJECT:lblDescrp:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblName */
        /*  */
        THIS-OBJECT:lblName:Location = NEW System.Drawing.Point(104, 362).
        THIS-OBJECT:lblName:Name = "lblName".
        THIS-OBJECT:lblName:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblName:TabIndex = 16.
        THIS-OBJECT:lblName:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblDescription */
        /*  */
        THIS-OBJECT:lblDescription:Location = NEW System.Drawing.Point(18, 410).
        THIS-OBJECT:lblDescription:Name = "lblDescription".
        THIS-OBJECT:lblDescription:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblDescription:TabIndex = 15.
        THIS-OBJECT:lblDescription:Text = "Description:".
        THIS-OBJECT:lblDescription:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblItemName */
        /*  */
        THIS-OBJECT:lblItemName:Location = NEW System.Drawing.Point(18, 362).
        THIS-OBJECT:lblItemName:Name = "lblItemName".
        THIS-OBJECT:lblItemName:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblItemName:TabIndex = 14.
        THIS-OBJECT:lblItemName:Text = "Item name:".
        THIS-OBJECT:lblItemName:UseCompatibleTextRendering = TRUE.
        /*  */
        /* btnAddToBasket */
        /*  */
        THIS-OBJECT:btnAddToBasket:Location = NEW System.Drawing.Point(12, 650).
        THIS-OBJECT:btnAddToBasket:Name = "btnAddToBasket".
        THIS-OBJECT:btnAddToBasket:Size = NEW System.Drawing.Size(233, 35).
        THIS-OBJECT:btnAddToBasket:TabIndex = 25.
        THIS-OBJECT:btnAddToBasket:Text = "Add to basket".
        THIS-OBJECT:btnAddToBasket:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnAddToBasket:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnAddToBasket:Click:Subscribe(THIS-OBJECT:button1_Click).
        /*  */
        /* pictureBox1 */
        /*  */
        THIS-OBJECT:pictureBox1:Location = NEW System.Drawing.Point(323, 362).
        THIS-OBJECT:pictureBox1:Name = "pictureBox1".
        THIS-OBJECT:pictureBox1:Size = NEW System.Drawing.Size(233, 199).
        THIS-OBJECT:pictureBox1:TabIndex = 26.
        THIS-OBJECT:pictureBox1:TabStop = FALSE.
        /*  */
        /* btnLoadImage */
        /*  */
        THIS-OBJECT:btnLoadImage:Enabled = FALSE.
        THIS-OBJECT:btnLoadImage:Location = NEW System.Drawing.Point(323, 612).
        THIS-OBJECT:btnLoadImage:Name = "btnLoadImage".
        THIS-OBJECT:btnLoadImage:Size = NEW System.Drawing.Size(233, 35).
        THIS-OBJECT:btnLoadImage:TabIndex = 27.
        THIS-OBJECT:btnLoadImage:Text = "Select image".
        THIS-OBJECT:btnLoadImage:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnLoadImage:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnLoadImage:Click:Subscribe(THIS-OBJECT:btnLoadImage_Click).
        /*  */
        /* btnSavePhoto */
        /*  */
        THIS-OBJECT:btnSavePhoto:Enabled = FALSE.
        THIS-OBJECT:btnSavePhoto:Location = NEW System.Drawing.Point(323, 650).
        THIS-OBJECT:btnSavePhoto:Name = "btnSavePhoto".
        THIS-OBJECT:btnSavePhoto:Size = NEW System.Drawing.Size(233, 34).
        THIS-OBJECT:btnSavePhoto:TabIndex = 28.
        THIS-OBJECT:btnSavePhoto:Text = "Save Photo".
        THIS-OBJECT:btnSavePhoto:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnSavePhoto:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnSavePhoto:Click:Subscribe(THIS-OBJECT:btnSavePhoto_Click).
        /*  */
        /* CreateANewOrder */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(572, 803).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnSavePhoto).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnLoadImage).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:pictureBox1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnAddToBasket).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblAllocated).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:label1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblQTYvalue).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblQty).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:numericUpDown1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPriceValue).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPrice).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblDescrp).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblName).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblDescription).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblItemName).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnShowBasket).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblSportType).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblEqType).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:comboBoxCategory1).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:comboBoxCategory2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:dataGridView1).
        THIS-OBJECT:Name = "CreateANewOrder".
        THIS-OBJECT:Text = "CreateANewOrder".
        CAST(THIS-OBJECT:dataGridView1, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:bsItemsToShow, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:bsCategory2, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:bsCategory, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:numericUpDown1, System.ComponentModel.ISupportInitialize):EndInit().
        CAST(THIS-OBJECT:pictureBox1, System.ComponentModel.ISupportInitialize):EndInit().
        THIS-OBJECT:ResumeLayout(FALSE).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    DESTRUCTOR PUBLIC CreateANewOrder ( ):

    END DESTRUCTOR.

END CLASS.