 
/*------------------------------------------------------------------------
   File        : LogInForm
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Thu Nov 12 10:20:39 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.FORMS.CreateAccountForm.CreateAccountForm FROM PROPATH.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING src.Classes.BusinessEntity.beCustomer FROM PROPATH.
USING src.FORMS.AdminView.AdminView FROM PROPATH.
USING src.FORMS.CustomerView.CustomerView FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.LogInFORM.LogInForm INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnClose          AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnLogIn          AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components        AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE txtUserName       AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE txtPassword       AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE linkCreateAccount AS System.Windows.Forms.LinkLabel   NO-UNDO.
    DEFINE PRIVATE VARIABLE lblUserName       AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPassword       AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE daCustomer        AS daCustomers.
    DEFINE PRIVATE VARIABLE beCustomer        AS beCustomer.
    DEFINE PRIVATE VARIABLE adminview         AS AdminView.
    DEFINE PRIVATE VARIABLE ViewCreateAccount AS CreateAccountForm.
    DEFINE PRIVATE VARIABLE EncodedPassword   AS CHARACTER.
    DEFINE PRIVATE VARIABLE customerView      AS CustomerView.
    CONSTRUCTOR PUBLIC LogInForm (  ):
        daCustomer = NEW daCustomers().
        InitializeComponent().
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        
          
          
          
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

	
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnClose_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    @VisualDesigner.
    METHOD PRIVATE VOID btnLogIn_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        
        daCustomer:FetchData(OUTPUT dataset dsCustomer BY-REFERENCE).
        
        beCustomer = NEW beCustomer().
        adminview = NEW AdminView().
        EncodedPassword = ENCODE(txtPassword:text).
        IF beCustomer:isAdmin(txtUserName:text,txtPassword:text) THEN 
        DO:
            adminview:Show().
        END.
        ELSE 
        DO:
            FIND FIRST ttCustomer WHERE ttCustomer.User_Name = txtUserName:Text NO-ERROR.
            
            IF AVAILABLE ttCustomer THEN 
            DO:
                IF ENCODE(txtPassword:text) EQ ttCustomer.Password THEN 
                DO:
                    customerView = NEW CustomerView(ttCustomer.CustNum).
                    customerView:Show().
                    
                END.
                IF EncodedPassword <> ttCustomer.Password THEN 
                DO:
                    MESSAGE "Password is not correct for this username"
                        VIEW-AS ALERT-BOX.    
                END.        
            END.
        
            ELSE MESSAGE "This username does not exist"
                    VIEW-AS ALERT-BOX.
        END.

        DATASET dsCustomer:EMPTY-DATASET ().        

    END METHOD.
    
    

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
        
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
        
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:txtUserName = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:txtPassword = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblUserName = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblPassword = NEW System.Windows.Forms.Label().
        THIS-OBJECT:linkCreateAccount = NEW System.Windows.Forms.LinkLabel().
        THIS-OBJECT:btnLogIn = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnClose = NEW System.Windows.Forms.Button().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* txtUserName */
        /*  */
        THIS-OBJECT:txtUserName:Location = NEW System.Drawing.Point(177, 39).
        THIS-OBJECT:txtUserName:Name = "txtUserName".
        THIS-OBJECT:txtUserName:Size = NEW System.Drawing.Size(153, 20).
        THIS-OBJECT:txtUserName:TabIndex = 0.
        THIS-OBJECT:txtUserName:TextChanged:Subscribe(THIS-OBJECT:txtUserName_TextChanged).
        /*  */
        /* txtPassword */
        /*  */
        THIS-OBJECT:txtPassword:Location = NEW System.Drawing.Point(177, 75).
        THIS-OBJECT:txtPassword:Name = "txtPassword".
        THIS-OBJECT:txtPassword:PasswordChar = '*'.
        THIS-OBJECT:txtPassword:Size = NEW System.Drawing.Size(153, 20).
        THIS-OBJECT:txtPassword:TabIndex = 1.
        THIS-OBJECT:txtPassword:WordWrap = FALSE.
        /*  */
        /* lblUserName */
        /*  */
        THIS-OBJECT:lblUserName:Location = NEW System.Drawing.Point(71, 39).
        THIS-OBJECT:lblUserName:Name = "lblUserName".
        THIS-OBJECT:lblUserName:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblUserName:TabIndex = 2.
        THIS-OBJECT:lblUserName:Text = "Username:".
        THIS-OBJECT:lblUserName:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblUserName:Click:Subscribe(THIS-OBJECT:lblUserName_Click).
        /*  */
        /* lblPassword */
        /*  */
        THIS-OBJECT:lblPassword:Location = NEW System.Drawing.Point(71, 72).
        THIS-OBJECT:lblPassword:Name = "lblPassword".
        THIS-OBJECT:lblPassword:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblPassword:TabIndex = 3.
        THIS-OBJECT:lblPassword:Text = "Password:".
        THIS-OBJECT:lblPassword:UseCompatibleTextRendering = TRUE.
        /*  */
        /* linkCreateAccount */
        /*  */
        THIS-OBJECT:linkCreateAccount:Location = NEW System.Drawing.Point(177, 247).
        THIS-OBJECT:linkCreateAccount:Name = "linkCreateAccount".
        THIS-OBJECT:linkCreateAccount:Size = NEW System.Drawing.Size(153, 27).
        THIS-OBJECT:linkCreateAccount:TabIndex = 4.
        THIS-OBJECT:linkCreateAccount:TabStop = TRUE.
        THIS-OBJECT:linkCreateAccount:Text = "If you don't have an account, click here to create one!".
        THIS-OBJECT:linkCreateAccount:TextAlign = System.Drawing.ContentAlignment:TopCenter.
        THIS-OBJECT:linkCreateAccount:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:linkCreateAccount:LinkClicked:Subscribe(THIS-OBJECT:linkCreateAccount_LinkClicked).
        /*  */
        /* btnLogIn */
        /*  */
        THIS-OBJECT:btnLogIn:Location = NEW System.Drawing.Point(216, 114).
        THIS-OBJECT:btnLogIn:Name = "btnLogIn".
        THIS-OBJECT:btnLogIn:Size = NEW System.Drawing.Size(75, 23).
        THIS-OBJECT:btnLogIn:TabIndex = 5.
        THIS-OBJECT:btnLogIn:Text = "Log In".
        THIS-OBJECT:btnLogIn:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnLogIn:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnLogIn:Click:Subscribe(THIS-OBJECT:btnLogIn_Click).
        /*  */
        /* btnClose */
        /*  */
        THIS-OBJECT:btnClose:Location = NEW System.Drawing.Point(216, 364).
        THIS-OBJECT:btnClose:Name = "btnClose".
        THIS-OBJECT:btnClose:Size = NEW System.Drawing.Size(75, 23).
        THIS-OBJECT:btnClose:TabIndex = 6.
        THIS-OBJECT:btnClose:Text = "Close".
        THIS-OBJECT:btnClose:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnClose:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnClose:Click:Subscribe(THIS-OBJECT:btnClose_Click).
        /*  */
        /* LogInForm */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(506, 399).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnClose).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnLogIn).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:linkCreateAccount).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblUserName).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:txtUserName).
        THIS-OBJECT:Name = "LogInForm".
        THIS-OBJECT:Text = "LogInForm".
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID lblUserName_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID linkCreateAccount_LinkClicked( INPUT sender AS System.Object, INPUT e AS System.Windows.Forms.LinkLabelLinkClickedEventArgs ):
		
        ViewCreateAccount = NEW CreateAccountForm().
        ViewCreateAccount:Show().
        

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID txtUserName_TextChanged( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC LogInForm ( ):

    END DESTRUCTOR.

END CLASS.