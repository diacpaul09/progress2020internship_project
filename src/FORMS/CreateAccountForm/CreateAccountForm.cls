 
/*------------------------------------------------------------------------
   File        : CreateAccountForm
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Thu Nov 12 11:59:46 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Progress.Windows.Form.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING OpenEdge.Core.Assert FROM PROPATH.
USING src.FORMS.LogInFORM.LogInForm FROM PROPATH.
USING src.Classes.Utils.StringValidator FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.FORMS.CreateAccountForm.CreateAccountForm INHERITS Form: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    DEFINE PRIVATE VARIABLE btnGoBackToLogIn           AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE btnCreate                  AS System.Windows.Forms.Button      NO-UNDO.
    DEFINE PRIVATE VARIABLE components                 AS System.ComponentModel.IContainer NO-UNDO.
    DEFINE PRIVATE VARIABLE lblAddress2                AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPhone                   AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblFax                     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblEmailAddress            AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCountry                 AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblCity                    AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE textAddress2               AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textAddress                AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblState                   AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPostalCode              AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblAddress                 AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE textState                  AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textPostalCode             AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textPhone                  AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textFax                    AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textEmailAddress           AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textCountry                AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textCity                   AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textUser_name              AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textPasswornd_confirmation AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textPassword               AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE textName                   AS System.Windows.Forms.TextBox     NO-UNDO.
    DEFINE PRIVATE VARIABLE lblUser_name               AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPasswordConfimation     AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblPassword                AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE lblFull_name               AS System.Windows.Forms.Label       NO-UNDO.
    DEFINE PRIVATE VARIABLE daCustomer                 AS daCustomers                      NO-UNDO.
    DEFINE PRIVATE VARIABLE loginform                  AS LogInForm                        NO-UNDO.
    DEFINE PRIVATE VARIABLE verLogical                 AS LOGICAL                          NO-UNDO.
    CONSTRUCTOR PUBLIC CreateAccountForm (  ):
        
        
        
        
        InitializeComponent().
        
        daCustomer = NEW daCustomers().
        daCustomer:FetchData(OUTPUT dataset dsCustomer).
        
        THIS-OBJECT:ComponentsCollection:Add(THIS-OBJECT:components).
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.

    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnCreate_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        assert:notEmpty(textUser_name:text).
        assert:notEmpty(textName:text).
        assert:notEmpty(textPassword:text).
        assert:notEmpty(textAddress:text).
        assert:notEmpty(textcity:text).
        assert:notEmpty(textCountry:text).
        assert:notEmpty(textEmailAddress:text).
        assert:notEmpty(textPhone:text).
        assert:notEmpty(textState:text).
        
        IF StringValidator:specialCharinUsername(textUser_name:text) THEN
        DO:
            MESSAGE "Username must not contain special characters"
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        
        IF LENGTH (textUser_name:text) < 6 THEN 
        DO:
            MESSAGE "Length of the username must be at least 6 characters long."
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
   
        FIND FIRST ttCustomer WHERE ttCustomer.User_Name EQ textUser_name:text NO-ERROR.
        IF AVAILABLE ttCustomer THEN 
        DO:
            MESSAGE "This username already exists."
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        
        IF  StringValidator:passwordNeedsSpecialChar(textPassword:text)  THEN
        DO:
            MESSAGE "Password must contain at least one special character"
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        
        IF LENGTH (textPassword:text) < 8 THEN 
        DO:
            MESSAGE "Length of the password must be at least 8 characters long."
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        
        IF textPassword:text NE textPasswornd_confirmation:text THEN 
        DO:
            MESSAGE "Passwords must match"
                VIEW-AS ALERT-BOX.
            UNDO, RETRY.
        END.
        
        IF StringValidator:charInPhoneNumber(textPhone:text) THEN
        DO:
            MESSAGE "Phone number must be composed only by numbers"
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        
        
        IF StringValidator:isEmailGood(textEmailAddress:text) <> TRUE THEN
        DO:
            MESSAGE "Your email format is not good"
                VIEW-AS ALERT-BOX.
            UNDO,RETRY.
        END.
        
        
        DATASET dsCustomer:EMPTY-DATASET ().
        
        CREATE ttCustomer.
        ASSIGN
            ttCustomer.CustNum      = daCustomer:lastCustomerNumber + 5
            ttCustomer.Name         = textName:text
            ttCustomer.Address      = textAddress:text
            ttCustomer.Address2     = textAddress2:text
            ttCustomer.City         = textCity:text
            ttCustomer.State        = textState:text
            ttCustomer.Country      = textCountry:text
            ttCustomer.EmailAddress = textEmailAddress:text
            ttCustomer.PostalCode   = textPostalCode:text
            ttCustomer.Phone        = textPhone:text
            ttCustomer.User_Name    = textUser_name:text
            ttCustomer.Password     = ENCODE(textPassword:text).
        RELEASE ttCustomer.
        daCustomer:StoreData(INPUT-OUTPUT DATASET dsCustomer).
        MESSAGE "Customer Account has been created"
            VIEW-AS ALERT-BOX.
      
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID btnGoBackToLogIn_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
        DELETE OBJECT THIS-OBJECT.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID CreateAccountForm_Load( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    METHOD PRIVATE VOID InitializeComponent(  ):
        
        /* NOTE: The following method is automatically generated.
                
        We strongly suggest that the contents of this method only be modified using the
        Visual Designer to avoid any incompatible modifications.
                
        Modifying the contents of this method using a code editor will invalidate any support for this file. */
        THIS-OBJECT:textName = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textUser_name = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textPassword = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textPasswornd_confirmation = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblFull_name = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblUser_name = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblPassword = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblPasswordConfimation = NEW System.Windows.Forms.Label().
        THIS-OBJECT:btnGoBackToLogIn = NEW System.Windows.Forms.Button().
        THIS-OBJECT:btnCreate = NEW System.Windows.Forms.Button().
        THIS-OBJECT:textAddress = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblAddress = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblAddress2 = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textAddress2 = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblCity = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textCity = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblCountry = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textCountry = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblEmailAddress = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textEmailAddress = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblFax = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textFax = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblPhone = NEW System.Windows.Forms.Label().
        THIS-OBJECT:textPhone = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textPostalCode = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:textState = NEW System.Windows.Forms.TextBox().
        THIS-OBJECT:lblPostalCode = NEW System.Windows.Forms.Label().
        THIS-OBJECT:lblState = NEW System.Windows.Forms.Label().
        THIS-OBJECT:SuspendLayout().
        /*  */
        /* textName */
        /*  */
        THIS-OBJECT:textName:Location = NEW System.Drawing.Point(205, 6).
        THIS-OBJECT:textName:Name = "textName".
        THIS-OBJECT:textName:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textName:TabIndex = 0.
        /*  */
        /* textUser_name */
        /*  */
        THIS-OBJECT:textUser_name:Location = NEW System.Drawing.Point(205, 32).
        THIS-OBJECT:textUser_name:Name = "textUser_name".
        THIS-OBJECT:textUser_name:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textUser_name:TabIndex = 1.
        /*  */
        /* textPassword */
        /*  */
        THIS-OBJECT:textPassword:Location = NEW System.Drawing.Point(205, 58).
        THIS-OBJECT:textPassword:Name = "textPassword".
        THIS-OBJECT:textPassword:PasswordChar = '*'.
        THIS-OBJECT:textPassword:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textPassword:TabIndex = 2.
        /*  */
        /* textPasswornd_confirmation */
        /*  */
        THIS-OBJECT:textPasswornd_confirmation:Location = NEW System.Drawing.Point(205, 84).
        THIS-OBJECT:textPasswornd_confirmation:Name = "textPasswornd_confirmation".
        THIS-OBJECT:textPasswornd_confirmation:PasswordChar = '*'.
        THIS-OBJECT:textPasswornd_confirmation:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textPasswornd_confirmation:TabIndex = 3.
        /*  */
        /* lblFull_name */
        /*  */
        THIS-OBJECT:lblFull_name:Location = NEW System.Drawing.Point(16, 9).
        THIS-OBJECT:lblFull_name:Name = "lblFull_name".
        THIS-OBJECT:lblFull_name:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblFull_name:TabIndex = 6.
        THIS-OBJECT:lblFull_name:Text = "*Full Name:".
        THIS-OBJECT:lblFull_name:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblFull_name:Click:Subscribe(THIS-OBJECT:lblFull_name_Click).
        /*  */
        /* lblUser_name */
        /*  */
        THIS-OBJECT:lblUser_name:Location = NEW System.Drawing.Point(16, 35).
        THIS-OBJECT:lblUser_name:Name = "lblUser_name".
        THIS-OBJECT:lblUser_name:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblUser_name:TabIndex = 7.
        THIS-OBJECT:lblUser_name:Text = "*Username:".
        THIS-OBJECT:lblUser_name:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblPassword */
        /*  */
        THIS-OBJECT:lblPassword:Location = NEW System.Drawing.Point(16, 61).
        THIS-OBJECT:lblPassword:Name = "lblPassword".
        THIS-OBJECT:lblPassword:Size = NEW System.Drawing.Size(100, 23).
        THIS-OBJECT:lblPassword:TabIndex = 8.
        THIS-OBJECT:lblPassword:Text = "*Password:".
        THIS-OBJECT:lblPassword:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblPasswordConfimation */
        /*  */
        THIS-OBJECT:lblPasswordConfimation:Location = NEW System.Drawing.Point(16, 87).
        THIS-OBJECT:lblPasswordConfimation:Name = "lblPasswordConfimation".
        THIS-OBJECT:lblPasswordConfimation:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblPasswordConfimation:TabIndex = 9.
        THIS-OBJECT:lblPasswordConfimation:Text = "*Password Confirmation:".
        THIS-OBJECT:lblPasswordConfimation:UseCompatibleTextRendering = TRUE.
        /*  */
        /* btnGoBackToLogIn */
        /*  */
        THIS-OBJECT:btnGoBackToLogIn:Location = NEW System.Drawing.Point(163, 465).
        THIS-OBJECT:btnGoBackToLogIn:Name = "btnGoBackToLogIn".
        THIS-OBJECT:btnGoBackToLogIn:Size = NEW System.Drawing.Size(178, 23).
        THIS-OBJECT:btnGoBackToLogIn:TabIndex = 5.
        THIS-OBJECT:btnGoBackToLogIn:Text = "Exit".
        THIS-OBJECT:btnGoBackToLogIn:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnGoBackToLogIn:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnGoBackToLogIn:Click:Subscribe(THIS-OBJECT:btnGoBackToLogIn_Click).
        /*  */
        /* btnCreate */
        /*  */
        THIS-OBJECT:btnCreate:Location = NEW System.Drawing.Point(163, 436).
        THIS-OBJECT:btnCreate:Name = "btnCreate".
        THIS-OBJECT:btnCreate:Size = NEW System.Drawing.Size(178, 23).
        THIS-OBJECT:btnCreate:TabIndex = 4.
        THIS-OBJECT:btnCreate:Text = "CreateAccount".
        THIS-OBJECT:btnCreate:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:btnCreate:UseVisualStyleBackColor = TRUE.
        THIS-OBJECT:btnCreate:Click:Subscribe(THIS-OBJECT:btnCreate_Click).
        /*  */
        /* textAddress */
        /*  */
        THIS-OBJECT:textAddress:Location = NEW System.Drawing.Point(205, 110).
        THIS-OBJECT:textAddress:Name = "textAddress".
        THIS-OBJECT:textAddress:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textAddress:TabIndex = 10.
        /*  */
        /* lblAddress */
        /*  */
        THIS-OBJECT:lblAddress:Location = NEW System.Drawing.Point(16, 113).
        THIS-OBJECT:lblAddress:Name = "lblAddress".
        THIS-OBJECT:lblAddress:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblAddress:TabIndex = 11.
        THIS-OBJECT:lblAddress:Text = "*Address:".
        THIS-OBJECT:lblAddress:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblAddress2 */
        /*  */
        THIS-OBJECT:lblAddress2:Location = NEW System.Drawing.Point(16, 139).
        THIS-OBJECT:lblAddress2:Name = "lblAddress2".
        THIS-OBJECT:lblAddress2:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblAddress2:TabIndex = 13.
        THIS-OBJECT:lblAddress2:Text = "Address2:".
        THIS-OBJECT:lblAddress2:UseCompatibleTextRendering = TRUE.
        THIS-OBJECT:lblAddress2:Click:Subscribe(THIS-OBJECT:label1_Click).
        /*  */
        /* textAddress2 */
        /*  */
        THIS-OBJECT:textAddress2:Location = NEW System.Drawing.Point(205, 136).
        THIS-OBJECT:textAddress2:Name = "textAddress2".
        THIS-OBJECT:textAddress2:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textAddress2:TabIndex = 12.
        THIS-OBJECT:textAddress2:TextChanged:Subscribe(THIS-OBJECT:textBox2_TextChanged).
        /*  */
        /* lblCity */
        /*  */
        THIS-OBJECT:lblCity:Location = NEW System.Drawing.Point(16, 165).
        THIS-OBJECT:lblCity:Name = "lblCity".
        THIS-OBJECT:lblCity:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblCity:TabIndex = 15.
        THIS-OBJECT:lblCity:Text = "*City:".
        THIS-OBJECT:lblCity:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textCity */
        /*  */
        THIS-OBJECT:textCity:Location = NEW System.Drawing.Point(205, 162).
        THIS-OBJECT:textCity:Name = "textCity".
        THIS-OBJECT:textCity:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textCity:TabIndex = 14.
        /*  */
        /* lblCountry */
        /*  */
        THIS-OBJECT:lblCountry:Location = NEW System.Drawing.Point(16, 191).
        THIS-OBJECT:lblCountry:Name = "lblCountry".
        THIS-OBJECT:lblCountry:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblCountry:TabIndex = 17.
        THIS-OBJECT:lblCountry:Text = "*Country".
        THIS-OBJECT:lblCountry:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textCountry */
        /*  */
        THIS-OBJECT:textCountry:Location = NEW System.Drawing.Point(205, 188).
        THIS-OBJECT:textCountry:Name = "textCountry".
        THIS-OBJECT:textCountry:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textCountry:TabIndex = 16.
        /*  */
        /* lblEmailAddress */
        /*  */
        THIS-OBJECT:lblEmailAddress:Location = NEW System.Drawing.Point(16, 217).
        THIS-OBJECT:lblEmailAddress:Name = "lblEmailAddress".
        THIS-OBJECT:lblEmailAddress:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblEmailAddress:TabIndex = 19.
        THIS-OBJECT:lblEmailAddress:Text = "*Email Address:".
        THIS-OBJECT:lblEmailAddress:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textEmailAddress */
        /*  */
        THIS-OBJECT:textEmailAddress:Location = NEW System.Drawing.Point(205, 214).
        THIS-OBJECT:textEmailAddress:Name = "textEmailAddress".
        THIS-OBJECT:textEmailAddress:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textEmailAddress:TabIndex = 18.
        /*  */
        /* lblFax */
        /*  */
        THIS-OBJECT:lblFax:Location = NEW System.Drawing.Point(16, 243).
        THIS-OBJECT:lblFax:Name = "lblFax".
        THIS-OBJECT:lblFax:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblFax:TabIndex = 21.
        THIS-OBJECT:lblFax:Text = "Fax".
        THIS-OBJECT:lblFax:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textFax */
        /*  */
        THIS-OBJECT:textFax:Location = NEW System.Drawing.Point(205, 240).
        THIS-OBJECT:textFax:Name = "textFax".
        THIS-OBJECT:textFax:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textFax:TabIndex = 20.
        /*  */
        /* lblPhone */
        /*  */
        THIS-OBJECT:lblPhone:Location = NEW System.Drawing.Point(16, 269).
        THIS-OBJECT:lblPhone:Name = "lblPhone".
        THIS-OBJECT:lblPhone:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblPhone:TabIndex = 23.
        THIS-OBJECT:lblPhone:Text = "*Phone number:".
        THIS-OBJECT:lblPhone:UseCompatibleTextRendering = TRUE.
        /*  */
        /* textPhone */
        /*  */
        THIS-OBJECT:textPhone:Location = NEW System.Drawing.Point(205, 266).
        THIS-OBJECT:textPhone:Name = "textPhone".
        THIS-OBJECT:textPhone:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textPhone:TabIndex = 22.
        /*  */
        /* textPostalCode */
        /*  */
        THIS-OBJECT:textPostalCode:Location = NEW System.Drawing.Point(205, 292).
        THIS-OBJECT:textPostalCode:Name = "textPostalCode".
        THIS-OBJECT:textPostalCode:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textPostalCode:TabIndex = 24.
        /*  */
        /* textState */
        /*  */
        THIS-OBJECT:textState:Location = NEW System.Drawing.Point(205, 318).
        THIS-OBJECT:textState:Name = "textState".
        THIS-OBJECT:textState:Size = NEW System.Drawing.Size(158, 20).
        THIS-OBJECT:textState:TabIndex = 25.
        /*  */
        /* lblPostalCode */
        /*  */
        THIS-OBJECT:lblPostalCode:Location = NEW System.Drawing.Point(16, 295).
        THIS-OBJECT:lblPostalCode:Name = "lblPostalCode".
        THIS-OBJECT:lblPostalCode:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblPostalCode:TabIndex = 26.
        THIS-OBJECT:lblPostalCode:Text = "Postal Code:".
        THIS-OBJECT:lblPostalCode:UseCompatibleTextRendering = TRUE.
        /*  */
        /* lblState */
        /*  */
        THIS-OBJECT:lblState:Location = NEW System.Drawing.Point(16, 321).
        THIS-OBJECT:lblState:Name = "lblState".
        THIS-OBJECT:lblState:Size = NEW System.Drawing.Size(175, 23).
        THIS-OBJECT:lblState:TabIndex = 27.
        THIS-OBJECT:lblState:Text = "*State:".
        THIS-OBJECT:lblState:UseCompatibleTextRendering = TRUE.
        /*  */
        /* CreateAccountForm */
        /*  */
        THIS-OBJECT:ClientSize = NEW System.Drawing.Size(505, 500).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblState).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPostalCode).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textState).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textPostalCode).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPhone).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textPhone).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblFax).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textFax).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblEmailAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textEmailAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCountry).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textCountry).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblCity).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textCity).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblAddress2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textAddress2).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textAddress).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPasswordConfimation).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblUser_name).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:lblFull_name).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnGoBackToLogIn).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:btnCreate).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textPasswornd_confirmation).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textPassword).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textUser_name).
        THIS-OBJECT:Controls:Add(THIS-OBJECT:textName).
        THIS-OBJECT:Name = "CreateAccountForm".
        THIS-OBJECT:Text = "CreateAccountForm".
        THIS-OBJECT:Load:Subscribe(THIS-OBJECT:CreateAccountForm_Load).
        THIS-OBJECT:ResumeLayout(FALSE).
        THIS-OBJECT:PerformLayout().
        CATCH e AS Progress.Lang.Error:
            UNDO, THROW e.
        END CATCH.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID label1_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID lblFull_name_Click( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    @VisualDesigner.
    METHOD PRIVATE VOID textBox2_TextChanged( INPUT sender AS System.Object, INPUT e AS System.EventArgs ):
		
        RETURN.

    END METHOD.

    DESTRUCTOR PUBLIC CreateAccountForm ( ):

    END DESTRUCTOR.

END CLASS.