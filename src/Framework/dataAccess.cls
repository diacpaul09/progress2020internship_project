 
 /*------------------------------------------------------------------------
    File        : dataAccess
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : pDiac
    Created     : Tue Nov 10 02:55:12 EET 2020
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Framework.iDataAccess.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Framework.dataAccess  IMPLEMENTS iDataAccess: 

    METHOD PUBLIC VOID FetchData( OUTPUT Dataset-handle DataHandle ):
        
    END METHOD.

    METHOD PUBLIC VOID StoreData( INPUT-OUTPUT Dataset-handle DataHandle ):
    END METHOD.

END CLASS.