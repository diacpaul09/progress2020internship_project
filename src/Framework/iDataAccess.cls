
/*------------------------------------------------------------------------
    File        : iDataAccess
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : pDiac
    Created     : Tue Nov 10 02:50:33 EET 2020
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

INTERFACE src.Framework.iDataAccess:  
    
    METHOD PUBLIC VOID FetchData( OUTPUT Dataset-handle DataHandle ).

    METHOD PUBLIC VOID StoreData(INPUT-OUTPUT Dataset-handle DataHandle ).    

END INTERFACE.