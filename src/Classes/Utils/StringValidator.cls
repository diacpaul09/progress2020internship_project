 

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.Utils.StringValidator: 



    

    METHOD PUBLIC STATIC LOGICAL charInPhoneNumber( INPUT str AS CHAR ):
        
        IF  INDEX(str,"a") NE 0 OR
            INDEX(str,"b") NE 0 OR
            INDEX(str,"c") NE 0 OR
            INDEX(str,"d") NE 0 OR
            INDEX(str,"e") NE 0 OR    
            INDEX(str,"f") NE 0 OR
            INDEX(str,"g") NE 0 OR
            INDEX(str,"h") NE 0 OR
            INDEX(str,"i") NE 0 OR
            INDEX(str,"j") NE 0 OR
            INDEX(str,"k") NE 0 OR
            INDEX(str,"l") NE 0 OR
            INDEX(str,"m") NE 0 OR
            INDEX(str,"n") NE 0 OR    
            INDEX(str,"o") NE 0 OR
            INDEX(str,"p") NE 0 OR
            INDEX(str,"q") NE 0 OR
            INDEX(str,"r") NE 0 OR
            INDEX(str,"s") NE 0 OR
            INDEX(str,"t") NE 0 OR
            INDEX(str,"u") NE 0 OR
            INDEX(str,"v") NE 0 OR
            INDEX(str,"w") NE 0 OR    
            INDEX(str,"x") NE 0 OR
            INDEX(str,"y") NE 0 OR
            INDEX(str,"z") NE 0 OR
            INDEX(str," ") NE 0 OR
            INDEX(str,"!") NE 0 OR
            INDEX(str,"@") NE 0 OR
            INDEX(str,"#") NE 0 OR
            INDEX(str,"$") NE 0 OR
            INDEX(str,"%") NE 0 OR
            INDEX(str,"^") NE 0 OR
            INDEX(str,"&") NE 0 OR
            INDEX(str,"*") NE 0 OR
            INDEX(str,"(") NE 0 OR
            INDEX(str,")") NE 0 OR
            INDEX(str,"-") NE 0 OR
            INDEX(str,"_") NE 0 OR
            INDEX(str,"=") NE 0 OR
            INDEX(str,"+") NE 0 OR
            INDEX(str,"]") NE 0 OR
            INDEX(str,"}") NE 0 OR
            INDEX(str,";") NE 0 OR
            INDEX(str,":") NE 0 OR
            INDEX(str,"'") NE 0 OR
            INDEX(str,"\") NE 0 OR
            INDEX(str,"|") NE 0 OR
            INDEX(str,",") NE 0 OR
            INDEX(str,"<") NE 0 OR
            INDEX(str,".") NE 0 OR
            INDEX(str,">") NE 0 OR
            INDEX(str,"/") NE 0 OR
            INDEX(str,"?") NE 0 THEN
            RETURN TRUE.
        ELSE
        
            RETURN FALSE.

    END METHOD.

    

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC STATIC LOGICAL isEmailGood( INPUT v-email AS CHARACTER):
        DEFINE VARIABLE nChar    AS INTEGER.
        DEFINE VARIABLE v-length AS INTEGER.
        DEFINE VARIABLE v-left   AS CHARACTER FORMAT "x(50)" NO-UNDO .
        DEFINE VARIABLE v-right  AS CHARACTER FORMAT "x(50)" NO-UNDO .
        DEFINE VARIABLE v-at     AS INTEGER.
        DEFINE VARIABLE v-dot    AS INTEGER.
        v-email = TRIM(v-email).
        v-length = LENGTH(v-email).
        IF v-length< 5 THEN /* Minimal acceptable email: X@X.X */
            RETURN FALSE.

        v-at = INDEX(v-email, "@").
        v-left = SUBSTRING (v-email, 1, (v-at - 1)).
        v-right = SUBSTRING(v-email, (v-at + 1), (v-length - (v-at ))).
        v-dot = INDEX(v-right,".").




        IF v-at = 0 OR v-dot = 0 OR length(v-left) = 0 OR length(v-right) = 0 THEN
        DO:
            RETURN FALSE.
        END.


        DO nChar = 1 TO LENGTH(v-left) :
            IF INDEX("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_" , CAPS(SUBSTRING(v-left,nChar,1))) = 0 THEN
            DO:
                RETURN FALSE.
            END.
        END.
        nChar = 0.
        DO nChar = 1 TO LENGTH(v-right) :
            IF INDEX("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-" , CAPS(SUBSTRING(v-right,nChar,1))) = 0 THEN
            DO:
                RETURN FALSE.
            END.
        END.

        RETURN TRUE. 
        
    END METHOD.

    METHOD PUBLIC STATIC LOGICAL passwordNeedsSpecialChar(INPUT str AS CHARACTER):
        
        IF  INDEX(str," ") EQ 0 AND
            INDEX(str,"!") EQ 0 AND
            INDEX(str,"@") EQ 0 AND
            INDEX(str,"#") EQ 0 AND
            INDEX(str,"$") EQ 0 AND
            INDEX(str,"%") EQ 0 AND
            INDEX(str,"^") EQ 0 AND
            INDEX(str,"&") EQ 0 AND
            INDEX(str,"*") EQ 0 AND
            INDEX(str,"(") EQ 0 AND
            INDEX(str,")") EQ 0 AND
            INDEX(str,"-") EQ 0 AND
            INDEX(str,"_") EQ 0 AND
            INDEX(str,"=") EQ 0 AND
            INDEX(str,"+") EQ 0 AND
            INDEX(str,"]") EQ 0 AND
            INDEX(str,"}") EQ 0 AND
            INDEX(str,";") EQ 0 AND
            INDEX(str,":") EQ 0 AND
            INDEX(str,"'") EQ 0 AND
            INDEX(str,"\") EQ 0 AND
            INDEX(str,"|") EQ 0 AND
            INDEX(str,",") EQ 0 AND
            INDEX(str,"<") EQ 0 AND
            INDEX(str,".") EQ 0 AND
            INDEX(str,">") EQ 0 AND
            INDEX(str,"/") EQ 0 AND
            INDEX(str,"?") EQ 0 THEN 
        DO:
            RETURN TRUE.
        END.
        ELSE 
        DO:
        
            RETURN FALSE.
        END.    
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC STATIC LOGICAL specialCharinUsername(INPUT str AS CHARACTER  ):
        
        IF  INDEX(str," ") NE 0 OR
            INDEX(str,"!") NE 0 OR
            INDEX(str,"@") NE 0 OR
            INDEX(str,"#") NE 0 OR
            INDEX(str,"$") NE 0 OR     
            INDEX(str,"%") NE 0 OR 
            INDEX(str,"^") NE 0 OR
            INDEX(str,"&") NE 0 OR
            INDEX(str,"*") NE 0 OR
            INDEX(str,"(") NE 0 OR
            INDEX(str,")") NE 0 OR
            INDEX(str,"-") NE 0 OR
            INDEX(str,"_") NE 0 OR
            INDEX(str,"=") NE 0 OR
            INDEX(str,"+") NE 0 OR
            INDEX(str,"]") NE 0 OR
            INDEX(str,"}") NE 0 OR
            INDEX(str,";") NE 0 OR
            INDEX(str,":") NE 0 OR
            INDEX(str,"'") NE 0 OR
            INDEX(str,"\") NE 0 OR
            INDEX(str,"|") NE 0 OR
            INDEX(str,",") NE 0 OR
            INDEX(str,"<") NE 0 OR
            INDEX(str,".") NE 0 OR
            INDEX(str,">") NE 0 OR
            INDEX(str,"/") NE 0 OR
            INDEX(str,"?") NE 0 THEN
            RETURN TRUE.
        ELSE RETURN FALSE.

    END METHOD.    
    

END CLASS.