 
/*------------------------------------------------------------------------
   File        : beOrderLine
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Tue Nov 17 09:18:14 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Classes.DataAcces.OrderLine.daOrderLine FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.BusinessEntity.beOrderLine: 
    {D:\Progress2020Internship_Project\src\Shared\dsOrderline.i}
    DEFINE PRIVATE VARIABLE daOrdeline AS daOrderLine NO-UNDO.

   


    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getOrderLines( INPUT ordernumber AS INTEGER, INPUT-OUTPUT dataset dsORderline  ):
        
        daOrdeline = NEW daOrderLine().
        daOrdeline:getOrdeLines(ordernumber, INPUT-OUTPUT dataset dsORderline BY-REFERENCE).

    END METHOD.

    METHOD PUBLIC VOID StoreData( INPUT-OUTPUT dataset dsORderline ):
        
        daOrdeline = NEW daOrderLine().
      
         
        daOrdeline:StoreData(INPUT-OUTPUT dataset dsORderline BY-REFERENCE).
      

    END METHOD.
    
           
    

END CLASS.