 
/*------------------------------------------------------------------------
   File        : beOrders
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Sun Nov 15 18:02:09 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Classes.DataAcces.Order.daOrder FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.BusinessEntity.beOrders: 
    {D:\Progress2020Internship_Project\src\Shared\dsOrder.i}
    DEFINE PRIVATE VARIABLE daOrders AS daOrder.
    
    
    



    METHOD PUBLIC INTEGER getLastOrderNum():
        daOrders = NEW daOrder().
        RETURN daOrders:getLastOrderNum().
            

    END METHOD.

    
    METHOD PUBLIC VOID StoreData( INPUT-OUTPUT dataset dsOrder ):
        
        daOrders = NEW daOrder().
      
         
        daOrders:StoreData(INPUT-OUTPUT dataset dsOrder BY-REFERENCE).
      

    END METHOD.

    METHOD PUBLIC VOID getOrders(  OUTPUT dataset dsOrder, INPUT customerNumber AS INTEGER):
        
        daOrders = NEW daOrder().
        daOrders:getOrders(OUTPUT dataset dsOrder BY-REFERENCE, customerNumber).

    END METHOD.

END CLASS.