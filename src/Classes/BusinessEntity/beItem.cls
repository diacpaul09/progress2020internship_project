 
/*------------------------------------------------------------------------
   File        : beItem
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Mon Nov 16 15:03:43 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Classes.DataAcces.Item.daItem FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.BusinessEntity.beItem: 
    {D:\Progress2020Internship_Project\src\Shared\dsItem.i}
    DEFINE PRIVATE VARIABLE daItem AS daItem NO-UNDO.
    

    CONSTRUCTOR PUBLIC beItem (  ):
        SUPER ().
        daItem = NEW daItem().
    END CONSTRUCTOR.

    

    METHOD PUBLIC VOID getItem( OUTPUT dataset dsItem ):
        
        daItem:FetchData(OUTPUT dataset dsItem BY-REFERENCE).

    END METHOD.


    METHOD PUBLIC VOID UpdateAllocated( INPUT itemnumber AS INTEGER, quantity AS INTEGER ):
        
        daItem:UpdateAllocated(itemnumber,quantity).

    END METHOD.
    


    DESTRUCTOR PUBLIC beItem ( ):

    END DESTRUCTOR.

END CLASS.