 
/*------------------------------------------------------------------------
   File        : beCustomer
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Fri Nov 13 15:45:15 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Classes.DataAcces.Customer.daCustomers FROM PROPATH.
USING OpenEdge.Core.Assert FROM PROPATH.
USING src.Classes.BusinessEntity.beCustomer FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.BusinessEntity.beCustomer: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    
    DEFINE PRIVATE VARIABLE daCustomer       AS daCustomers.
    DEFINE PRIVATE VARIABLE dsCustomerHandle AS HANDLE      NO-UNDO.
   

    CONSTRUCTOR PUBLIC beCustomer (  ):
        daCustomer = NEW daCustomers().
        dsCustomerHandle = DATASET dsCustomer:HANDLE.
              
    END CONSTRUCTOR.

    

    

    METHOD PUBLIC VOID DeleteCustomer(INPUT customernumber AS INTEGER  ):
        
        daCustomer:DeleteCustomer(customernumber).

    END METHOD.
    
  
    METHOD PUBLIC VOID getCustomer( OUTPUT DATASET dsCustomer,INPUT customernumber AS INTEGER ):
        
        
        daCustomer:getCustomer(OUTPUT dataset dsCustomer BY-REFERENCE, customernumber).

    END METHOD.

    
    

    METHOD PUBLIC VOID UpdateCustomer(INPUT-OUTPUT DATASET dsCustomer ):
        
        
        daCustomer:UpdateCustomer(INPUT-OUTPUT dataset dsCustomer BY-REFERENCE) .           
        

    END METHOD.
    
    METHOD PUBLIC VOID UpdateCustomerPassword(INPUT-OUTPUT DATASET dsCustomer ):              
        
        daCustomer:UpdateCustomePassword(INPUT-OUTPUT dataset dsCustomer BY-REFERENCE).

    END METHOD.

    METHOD PUBLIC LOGICAL isAdmin(username AS CHARACTER, password AS CHARACTER):
        
        assert:notequal(username, "").
        assert:notequal(username, ?).
        assert:notequal(password, "").
        assert:notequal(password, ?).
        
       
        RETURN daCustomer:isAdmin(username,password).

    END METHOD.
    
    
    METHOD PUBLIC LOGICAL notUsernameFree(INPUT username AS CHARACTER ):              
        
        RETURN daCustomer:notUsernameFree(username).

    END METHOD.


    DESTRUCTOR PUBLIC beCustomer ( ):

    END DESTRUCTOR.

END CLASS.