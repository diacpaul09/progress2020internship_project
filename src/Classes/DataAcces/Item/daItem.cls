 
/*------------------------------------------------------------------------
   File        : daItem
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Mon Nov 16 14:28:12 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Framework.dataAccess FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.DataAcces.Item.daItem INHERITS dataAccess: 
    {D:\Progress2020Internship_Project\src\Shared\dsItem.i}
    
    
   

    CONSTRUCTOR PUBLIC daItem (  ):
        SUPER ().
        
    END CONSTRUCTOR.


    METHOD OVERRIDE PUBLIC VOID FetchData(  OUTPUT Dataset-handle DataHandle ):
        
        getItem(INPUT-OUTPUT dataset-handle DataHandle BY-REFERENCE).

    END METHOD.
    
    METHOD PUBLIC VOID UpdateAllocated( INPUT itemnumber AS INTEGER, quantity AS INTEGER ):
        
        FIND FIRST Item EXCLUSIVE-LOCK WHERE Item.ItemNum = itemnumber NO-ERROR.
        IF AVAILABLE Item THEN
            Item.Allocated = Item.Allocated - quantity.

    END METHOD.



    METHOD PUBLIC VOID getItem  (INPUT-OUTPUT dataset dsItem):
        FOR EACH Item NO-LOCK BY Item.ItemNum:
            CREATE ttItem.
            BUFFER-COPY Item TO ttItem.
            RELEASE ttItem.
        
        END.
        
        DATASET dsItem:WRITE-XML("file","D:\Progress2020Internship_Project\src\CHECK\dsItem.xml",TRUE).
        
    END METHOD.



END CLASS.