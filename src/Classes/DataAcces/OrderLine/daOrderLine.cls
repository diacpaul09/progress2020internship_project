 
/*------------------------------------------------------------------------
   File        : daOrderLine
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : pDiac
   Created     : Tue Nov 17 14:22:01 EET 2020
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING src.Framework.dataAccess.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.DataAcces.OrderLine.daOrderLine INHERITS dataAccess: 
    {D:\Progress2020Internship_Project\src\Shared\dsOrderline.i}





    

    METHOD PUBLIC VOID getOrdeLines( INPUT ordernumber AS INTEGER, INPUT-OUTPUT dataset dsORderline  ):
        
        FOR EACH OrderLine WHERE OrderLine.OrderNum = ordernumber :
            CREATE ttOrderLine.
            BUFFER-COPY OrderLine TO ttOrderLine.
            RELEASE ttOrderLine.
        END. 
        

    END METHOD.

    METHOD OVERRIDE PUBLIC VOID StoreData(INPUT-OUTPUT Dataset-handle DataHandle):
        DEFINE VARIABLE orderLineBufferHandle      AS HANDLE NO-UNDO.
        DEFINE VARIABLE queryOrderLineBufferHandle AS HANDLE NO-UNDO.
        
        orderLineBufferHandle = DataHandle:get-buffer-handle("ttOrderLine").
        
        CREATE QUERY queryOrderLineBufferHandle.
        queryOrderLineBufferHandle:SET-BUFFERS(orderLineBufferHandle).
        queryOrderLineBufferHandle:QUERY-PREPARE("for each ttOrderLine").
        queryOrderLineBufferHandle:FORWARD-ONLY = TRUE.
        queryOrderLineBufferHandle:QUERY-OPEN().
        
        
        REPEAT:
      
            queryOrderLineBufferHandle:GET-NEXT().
            IF (queryOrderLineBufferHandle:QUERY-OFF-END) THEN 
                LEAVE.
  
          
            CREATE OrderLine.
            ASSIGN
                OrderLine.ExtendedPrice   = orderLineBufferHandle::ExtendedPrice
                OrderLine.ItemNum         = orderLineBufferHandle::ItemNum
                OrderLine.LineNum         = orderLineBufferHandle::LineNum
                OrderLine.OrderLineStatus = orderLineBufferHandle::OrderLineStatus
                OrderLine.OrderNum        = orderLineBufferHandle::OrderNum
                OrderLine.Price           = orderLineBufferHandle::Price
                OrderLine.Qty             = orderLineBufferHandle::Qty.
            RELEASE Order.
       
            
       
       
        END.
    END METHOD.


END CLASS.