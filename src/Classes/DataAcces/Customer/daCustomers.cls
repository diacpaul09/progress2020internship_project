 


USING Progress.Lang.*.
USING src.Framework.dataAccess.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.DataAcces.Customer.daCustomers INHERITS dataAccess: 
    {D:\Progress2020Internship_Project\src\Shared\dsCustomer.i}
    
    DEFINE PUBLIC PROPERTY lastCustomerNumber AS INTEGER NO-UNDO 
        GET.
        PRIVATE SET.
        
       
   
    CONSTRUCTOR PUBLIC daCustomers (  ):
        SUPER ().


        FOR EACH customer NO-LOCK BY Customer.CustNum :
            lastCustomerNumber = Customer.CustNum.
        END.
         
        
    END CONSTRUCTOR.

    

  

    METHOD OVERRIDE PUBLIC VOID FetchData(  OUTPUT Dataset-handle DataHandle ):
        
        getCustomer(INPUT-OUTPUT dataset-handle DataHandle BY-REFERENCE).

    END METHOD.

    METHOD PUBLIC VOID getCustomer(INPUT-OUTPUT dataset dsCustomer):
        FOR EACH customer NO-LOCK BY Customer.CustNum:
            CREATE ttCustomer.
            BUFFER-COPY customer TO ttCustomer.
            RELEASE ttCustomer.
            lastCustomerNumber = Customer.CustNum.
        END.
        
    END METHOD.
    
    METHOD  PUBLIC VOID getCustomer(OUTPUT dataset dsCustomer, customernumber AS INTEGER):
        
        FIND FIRST customer WHERE Customer.CustNum = customernumber NO-LOCK NO-ERROR.
        IF AVAILABLE Customer THEN 
        DO:
            CREATE ttCustomer.
            BUFFER-COPY customer TO ttCustomer.
            RELEASE ttCustomer.
        END.
        
        
        
    END METHOD.
    
    
    
   
    METHOD PUBLIC LOGICAL isAdmin( username AS CHARACTER, password AS CHARACTER ):
        
        RETURN CAN-FIND (FIRST customer WHERE Customer.User_Name = username AND Customer.Password = password AND password = "admin" AND username = "admin" NO-LOCK ).
   
    END METHOD.

    METHOD OVERRIDE PUBLIC VOID StoreData(INPUT-OUTPUT Dataset-handle DataHandle):
        DEFINE VARIABLE customerBufferHandle AS HANDLE NO-UNDO.
        
    
        
        customerBufferHandle = DataHandle:get-buffer-handle("ttCustomer").
        
        customerBufferHandle:FIND-FIRST ().
        
        IF customerBufferHandle:AVAILABLE THEN 
        DO:
           //assert
            CREATE Customer.
            ASSIGN
                Customer.CustNum      = customerBufferHandle::CustNum
                Customer.Address      = customerBufferHandle::Address
                Customer.Address2     = customerBufferHandle::Address2
                Customer.Name         = customerBufferHandle::NAME
                Customer.City         = customerBufferHandle::City
                Customer.State        = customerBufferHandle::State
                Customer.Country      = customerBufferHandle::Country
                Customer.EmailAddress = customerBufferHandle::EmailAddress
                Customer.PostalCode   = customerBufferHandle::PostalCode
                Customer.Phone        = customerBufferHandle::Phone
                Customer.User_Name    = customerBufferHandle::User_Name
                Customer.Password     = customerBufferHandle::Password.
                
            RELEASE customer.
       
        END.
       
    END METHOD.

   

    METHOD PUBLIC VOID UpdateCustomer(INPUT-OUTPUT DATASET dsCustomer):
       
                                      
        FIND FIRST ttCustomer  NO-ERROR.
        
        
        FIND FIRST customer EXCLUSIVE-LOCK WHERE Customer.CustNum = ttCustomer.CustNum NO-ERROR.
        
        BUFFER-COPY ttCustomer EXCEPT CustNum  TO Customer .
       
                            
    END METHOD.
    
    METHOD PUBLIC LOGICAL notUsernameFree(INPUT username AS CHARACTER):

        FIND FIRST Customer NO-LOCK WHERE Customer.User_Name = username NO-ERROR.
        
        IF AVAILABLE customer THEN 
        DO:
           
            RETURN TRUE.
        END.
        ELSE RETURN FALSE.


    END METHOD.    
    
    METHOD PUBLIC VOID UpdateCustomePassword(INPUT-OUTPUT dataset dsCustomer):
        
        FIND FIRST ttCustomer NO-ERROR.
        
        FIND FIRST Customer EXCLUSIVE-LOCK WHERE Customer.CustNum = ttCustomer.CustNum NO-ERROR.
        
        
        
        ASSIGN 
            Customer.Password = ENCODE(ttCustomer.Password).
            
           
    END METHOD.

    METHOD PUBLIC VOID DeleteCustomer( INPUT customernumber AS INTEGER ):
    
        FIND FIRST customer EXCLUSIVE-LOCK WHERE Customer.CustNum = customernumber.
        DELETE customer.

    END METHOD.


END CLASS.


