
USING Progress.Lang.*.
USING src.Framework.dataAccess FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS src.Classes.DataAcces.Order.daOrder INHERITS dataAccess: 
    {D:\Progress2020Internship_Project\src\Shared\dsOrder.i}
    DEFINE VARIABLE lastOrderNum AS INTEGER.
    

    CONSTRUCTOR PUBLIC daOrder (  ):
        SUPER ().
        
       
        FOR EACH Order BY Order.OrderNum:
            lastOrderNum = Order.OrderNum.
        END.    
    END CONSTRUCTOR.


    METHOD PUBLIC INTEGER getLastOrderNum( ):
  
        RETURN lastOrderNum.

    END METHOD.



    METHOD PUBLIC VOID getOrders(OUTPUT dataset dsOrder, INPUT  customerNumber AS INTEGER):
        
        
        FOR EACH order NO-LOCK WHERE Order.CustNum = customerNumber:
            CREATE ttOrder.
            BUFFER-COPY order TO ttOrder.
            RELEASE ttOrder.
        END.
        
    END METHOD.
    
    
    METHOD OVERRIDE PUBLIC VOID StoreData(INPUT-OUTPUT Dataset-handle DataHandle):
        DEFINE VARIABLE orderBufferHandle AS HANDLE NO-UNDO.

        orderBufferHandle = DataHandle:get-buffer-handle("ttOrder").
        
        orderBufferHandle:FIND-FIRST ().
        
        IF orderBufferHandle:AVAILABLE THEN 
        DO:
           //assert
            CREATE Order.
            ASSIGN
                Order.Carrier     = orderBufferHandle::Carrier
                Order.CreditCard  = orderBufferHandle::CreditCard
                Order.CustNum     = orderBufferHandle::CustNum
                Order.OrderNum    = orderBufferHandle::OrderNum
                Order.OrderDate   = orderBufferHandle::OrderDate
                Order.OrderStatus = orderBufferHandle::OrderStatus
                Order.PromiseDate = orderBufferHandle::PromiseDate.
                        

            RELEASE Order.
       
        END.
       
    END METHOD.
    
    
    

END CLASS.